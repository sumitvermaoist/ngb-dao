package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.AdditionalSecurityDepositDAOInterface;
import com.mppkvvcl.ngbdao.repositories.AdditionalSecurityDepositRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.AdditionalSecurityDepositInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by PREETESH on 11/18/2017.
 */
@Service
public class AdditionalSecurityDepositDAO implements AdditionalSecurityDepositDAOInterface<AdditionalSecurityDepositInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(AdditionalSecurityDepositDAO.class);

    @Autowired
    private AdditionalSecurityDepositRepository additionalSecurityDepositRepository;

    @Override
    public List<AdditionalSecurityDepositInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        List<AdditionalSecurityDepositInterface> additionalSecurityDepositInterface = null;
        if(consumerNo != null){
            additionalSecurityDepositInterface = additionalSecurityDepositRepository.findByConsumerNo(consumerNo);
        }
        return additionalSecurityDepositInterface;
    }

    @Override
    public AdditionalSecurityDepositInterface get(AdditionalSecurityDepositInterface additionalSecurityDeposit) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        AdditionalSecurityDepositInterface existingBill = null;
        if(additionalSecurityDeposit != null){
            existingBill = additionalSecurityDepositRepository.findOne(additionalSecurityDeposit.getId());
        }
        return existingBill;
    }

    @Override
    public AdditionalSecurityDepositInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        AdditionalSecurityDepositInterface additionalSecurityDeposit = null;
        additionalSecurityDeposit = additionalSecurityDepositRepository.findOne(id);
        return additionalSecurityDeposit;
    }

    @Override
    public AdditionalSecurityDepositInterface add(AdditionalSecurityDepositInterface additionalSecurityDepositToAdd) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        AdditionalSecurityDepositInterface insertedAdditionalSecurityDeposit = null;
        if(additionalSecurityDepositToAdd != null){
            insertedAdditionalSecurityDeposit = additionalSecurityDepositRepository.save(additionalSecurityDepositToAdd);
        }
        return insertedAdditionalSecurityDeposit;
    }

    @Override
    public AdditionalSecurityDepositInterface update(AdditionalSecurityDepositInterface additionalSecurityDepositToUpdate) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        AdditionalSecurityDepositInterface updatedAdditionalSecurityDeposit = null;
        if(additionalSecurityDepositToUpdate != null){
            updatedAdditionalSecurityDeposit = additionalSecurityDepositRepository.save(additionalSecurityDepositToUpdate);
        }
        return updatedAdditionalSecurityDeposit;
    }

    @Override
    public AdditionalSecurityDepositInterface getByConsumerNoAndBillMonth(String consumerNo, String billMonth) {
        final String methodName = "getByConsumerNoAndBillMonth() : ";
        logger.info(methodName + "called");
        AdditionalSecurityDepositInterface additionalSecurityDeposit = null;
        if(consumerNo != null && billMonth != null ){
            additionalSecurityDeposit = additionalSecurityDepositRepository.findByConsumerNoAndBillMonth(consumerNo,billMonth);
        }
        return additionalSecurityDeposit;
    }

}
