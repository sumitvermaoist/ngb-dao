package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.AgricultureGMCDAOInterface;
import com.mppkvvcl.ngbdao.repositories.AgricultureGMCRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.AgricultureGMCInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AgricultureGMCDAO implements AgricultureGMCDAOInterface<AgricultureGMCInterface> {

    private Logger logger = GlobalResources.getLogger(AgricultureGMCDAO.class);

    @Autowired
    private AgricultureGMCRepository agricultureGMCRepository;


    @Override
    public AgricultureGMCInterface getByTariffIdAndSubcategoryCodeAndBillMonth(long tariffId, long subcategoryCode, String billMonth) {
        final String methodName = "getByTariffIdAndSubcategoryCodeAndBillMonth() : ";
        logger.info(methodName + "called");
        AgricultureGMCInterface agricultureGMCInterface = null;
        if(billMonth != null){
            agricultureGMCInterface = agricultureGMCRepository.findByTariffIdAndSubcategoryCodeAndBillMonth(tariffId,subcategoryCode,billMonth);
        }
        return agricultureGMCInterface;
    }

    @Override
    public List<AgricultureGMCInterface> getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode) {
        final String methodName = "getByTariffIdAndSubcategoryCode() : ";
        logger.info(methodName + "called");
        List<AgricultureGMCInterface> agricultureGMCInterfaces = agricultureGMCRepository.findByTariffIdAndSubcategoryCode(tariffId,subcategoryCode);
        return agricultureGMCInterfaces;
    }

    @Override
    public AgricultureGMCInterface add(AgricultureGMCInterface agricultureGMCInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        AgricultureGMCInterface insertedAgricultureGMCInterface = null;
        if(agricultureGMCInterface != null){
            insertedAgricultureGMCInterface = agricultureGMCRepository.save(agricultureGMCInterface);
        }
        return insertedAgricultureGMCInterface;
    }

    @Override
    public AgricultureGMCInterface update(AgricultureGMCInterface agricultureGMCInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        AgricultureGMCInterface updatedAgricultureGMCInterface = null;
        if(agricultureGMCInterface != null){
            updatedAgricultureGMCInterface = agricultureGMCRepository.save(agricultureGMCInterface);
        }
        return updatedAgricultureGMCInterface;
    }

    @Override
    public AgricultureGMCInterface get(AgricultureGMCInterface agricultureGMCInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        AgricultureGMCInterface fetchedAgricultureGMCInterface = null;
        if(agricultureGMCInterface != null){
            fetchedAgricultureGMCInterface = agricultureGMCRepository.findOne(agricultureGMCInterface.getId());
        }
        return fetchedAgricultureGMCInterface;
    }

    @Override
    public AgricultureGMCInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        AgricultureGMCInterface agricultureGMCInterface = agricultureGMCRepository.findOne(id);
        return agricultureGMCInterface;
    }
}
