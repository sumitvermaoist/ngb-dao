package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbinterface.interfaces.AgricultureUnitInterface;
import com.mppkvvcl.ngbdao.interfaces.AgricultureUnitDAOInterface;
import com.mppkvvcl.ngbdao.repositories.AgricultureUnitRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ANSHIKA on 16-09-2017.
 */
@Service
public class AgricultureUnitDAO implements AgricultureUnitDAOInterface<AgricultureUnitInterface> {

    /**
     * Requesting spring to get singleton AgricultureUnitRepository object.
     */
    @Autowired
    private AgricultureUnitRepository agricultureUnitRepository;

    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(AgricultureUnitDAO.class);

    @Override
    public AgricultureUnitInterface getBySubcategoryCodeAndBillMonth(long subcategoryCode, String billMonth) {
        final String methodName = "getBySubcategoryCodeAndBillMonth() : ";
        logger.info(methodName + "is called");
        AgricultureUnitInterface agricultureUnit = null;
        if(subcategoryCode > 0 && billMonth != null){
            agricultureUnit = agricultureUnitRepository.findBySubcategoryCodeAndBillMonth(subcategoryCode, billMonth);
        }
        return agricultureUnit;
    }

    @Override
    public AgricultureUnitInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        AgricultureUnitInterface agricultureUnit = null;
        agricultureUnit = agricultureUnitRepository.findOne(id);
        return agricultureUnit;
    }

    @Override
    public AgricultureUnitInterface add(AgricultureUnitInterface agricultureUnit) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        AgricultureUnitInterface insertedAgricultureUnit = null;
        if(agricultureUnit != null) {
            insertedAgricultureUnit = agricultureUnitRepository.save(agricultureUnit);
        }
        return insertedAgricultureUnit;
    }

    @Override
    public AgricultureUnitInterface get(AgricultureUnitInterface agricultureUnit) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        AgricultureUnitInterface existingAgricultureUnit = null;
        if(agricultureUnit != null) {
            existingAgricultureUnit = agricultureUnitRepository.findOne(agricultureUnit.getId());
        }
        return agricultureUnit;
    }

    @Override
    public AgricultureUnitInterface update(AgricultureUnitInterface agricultureUnit) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        AgricultureUnitInterface updatedAgricultureUnit = null;
        if(agricultureUnit != null) {
            updatedAgricultureUnit = agricultureUnitRepository.save(agricultureUnit);
        }
        return updatedAgricultureUnit;
    }
}
