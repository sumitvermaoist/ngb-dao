package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConnectionTypeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConnectionTypeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConnectionTypeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class ConnectionTypeDAO implements ConnectionTypeDAOInterface<ConnectionTypeInterface> {
    @Autowired
    private ConnectionTypeRepository connectionTypeRepository;
    /**
     *
     */
    private Logger logger = GlobalResources.getLogger(ConnectionTypeDAO.class);

    @Override
    public ConnectionTypeInterface add(ConnectionTypeInterface connectionType) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ConnectionTypeInterface insertedConnectionType = null;
        if(connectionType != null){
            insertedConnectionType = connectionTypeRepository.save(connectionType);
        }
        return insertedConnectionType;
    }

    @Override
    public ConnectionTypeInterface get(ConnectionTypeInterface connectionType) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ConnectionTypeInterface existingConnectionType = null;
        if(connectionType != null){
            existingConnectionType = connectionTypeRepository.findOne(connectionType.getId());
        }
        return existingConnectionType;
    }

    @Override
    public ConnectionTypeInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ConnectionTypeInterface connectionType = null;
        if(id > 0 ){
            connectionType = connectionTypeRepository.findOne(id);
        }
        return connectionType;
    }

    @Override
    public ConnectionTypeInterface update(ConnectionTypeInterface connectionType) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ConnectionTypeInterface updatedConnectionType = null;
        if(connectionType != null){
            updatedConnectionType = connectionTypeRepository.save(connectionType);
        }
        return updatedConnectionType;
    }

    @Override
    public List<? extends ConnectionTypeInterface> getAll() {
        final String methodName = "getAll() : ";
        logger.info(methodName + "called");
        List<? extends ConnectionTypeInterface> connectionTypes = null;
        connectionTypes = connectionTypeRepository.findAll();
        return connectionTypes;

    }
}
