package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerConnectionAreaInformationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerConnectionAreaInformationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionAreaInformationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class ConsumerConnectionAreaInformationDAO implements ConsumerConnectionAreaInformationDAOInterface<ConsumerConnectionAreaInformationInterface> {

    private Logger logger = GlobalResources.getLogger(ConsumerConnectionAreaInformationDAO.class);

    @Autowired
    private ConsumerConnectionAreaInformationRepository consumerConnectionAreaInformationRepository;

    @Override
    public ConsumerConnectionAreaInformationInterface getByConsumerNo(String consumerNumber) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = null;
        if(consumerNumber != null){
            consumerConnectionAreaInformation = consumerConnectionAreaInformationRepository.findByConsumerNo(consumerNumber);
        }
        return consumerConnectionAreaInformation;
    }

    @Override
    public List<ConsumerConnectionAreaInformationInterface> getByDtrName(String dtrName) {
        final String methodName = "getByDtrName() : ";
        logger.info(methodName + " called");
        List<ConsumerConnectionAreaInformationInterface> consumerConnectionAreaInformations = null;
        if(dtrName != null){
            consumerConnectionAreaInformations = consumerConnectionAreaInformationRepository.findByDtrName(dtrName);
        }
        return consumerConnectionAreaInformations;
    }

    @Override
    public List<ConsumerConnectionAreaInformationInterface> getByFeederName(String feederName) {
        final String methodName = "getByFeederName() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionAreaInformationInterface> consumerConnectionAreaInformations = null;
        if(feederName != null){
            consumerConnectionAreaInformations = consumerConnectionAreaInformationRepository.findByFeederName(feederName);
        }
        return consumerConnectionAreaInformations;
    }

    @Override
    public List<ConsumerConnectionAreaInformationInterface> getByFeederNameAndAreaStatus(String feederName, String areaStatus) {
        final String methodName = "getByFeederNameAndAreaStatus() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionAreaInformationInterface> consumerConnectionAreaInformations = null;
        if(feederName != null && areaStatus != null){
            consumerConnectionAreaInformations = consumerConnectionAreaInformationRepository.findByFeederNameAndAreaStatus(feederName, areaStatus);
        }
        return consumerConnectionAreaInformations;
    }

    @Override
    public long countByGroupNo(String groupNo) {
        final String methodName = "countByGroupNo() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null){
            count = consumerConnectionAreaInformationRepository.countByGroupNo(groupNo);
        }
        return count;
    }

    @Override
    public long countByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo) {
        final String methodName = "countByGroupNoAndReadingDiaryNo() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null && readingDiaryNo != null){
            count = consumerConnectionAreaInformationRepository.countByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo);
        }
        return count;
    }

    @Override
    public List<ConsumerConnectionAreaInformationInterface> getByFeederNameAndDtrNameAndPoleNo(String feederName, String dtrName, String poleNo) {
        final String methodName = "getByFeederNameAndDtrNameAndPoleNo() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionAreaInformationInterface> consumerConnectionAreaInformations = null;
        if(feederName != null && dtrName != null && poleNo != null){
            consumerConnectionAreaInformations = consumerConnectionAreaInformationRepository.findByFeederNameAndDtrNameAndPoleNo(feederName, dtrName, poleNo);
        }
        return consumerConnectionAreaInformations;
    }

    @Override
    public List<ConsumerConnectionAreaInformationInterface> getByGroupNo(String groupNo) {
        final String methodName = "getByGroupNo() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionAreaInformationInterface> consumerConnectionAreaInformations = null;
        if(groupNo != null){
            consumerConnectionAreaInformations = consumerConnectionAreaInformationRepository.findByGroupNo(groupNo);
        }
        return consumerConnectionAreaInformations;
    }

    @Override
    public List<ConsumerConnectionAreaInformationInterface> getByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo) {
        final String methodName = "getByGroupNoAndReadingDiaryNo() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionAreaInformationInterface> consumerConnectionAreaInformations = null;
        if(groupNo != null && readingDiaryNo != null){
            consumerConnectionAreaInformations = consumerConnectionAreaInformationRepository.findByGroupNoAndReadingDiaryNo(groupNo, readingDiaryNo);
        }
        return consumerConnectionAreaInformations;
    }

    @Override
    public List<ConsumerConnectionAreaInformationInterface> getByFeederNameAndDtrName(String feederName, String dtrName) {
        final String methodName = "getByFeederNameAndDtrName() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionAreaInformationInterface> consumerConnectionAreaInformations = null;
        if(feederName != null && dtrName != null){
            consumerConnectionAreaInformations = consumerConnectionAreaInformationRepository.findByFeederNameAndDtrName(feederName, dtrName);
        }
        return consumerConnectionAreaInformations;
    }

    @Override
    public List<ConsumerConnectionAreaInformationInterface> getBySurveyDate(Date surveyDate) {
        final String methodName = "getBySurveyDate() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionAreaInformationInterface> consumerConnectionAreaInformations = null;
        if (surveyDate != null){
            consumerConnectionAreaInformations = consumerConnectionAreaInformationRepository.findBySurveyDate(surveyDate);
        }
        return consumerConnectionAreaInformations;
    }

    @Override
    public List<ConsumerConnectionAreaInformationInterface> getBySurveyDateBetween(Date startSurveyDate, Date endSurveyDate) {
        final String methodName = "getBySurveyDateBetween() : ";
        logger.info(methodName + " called");
        List<ConsumerConnectionAreaInformationInterface> consumerConnectionAreaInformations = null;
        if(startSurveyDate != null && endSurveyDate != null){
            consumerConnectionAreaInformations = consumerConnectionAreaInformationRepository.findBySurveyDateBetween(startSurveyDate, endSurveyDate);
        }
        return consumerConnectionAreaInformations;
    }


    @Override
    public ConsumerConnectionAreaInformationInterface add(ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation) {
        final String methodName = "add() : ";
        logger.info(methodName + " called");
        ConsumerConnectionAreaInformationInterface insertedConsumerConnectionAreaInformation = null;
        if(consumerConnectionAreaInformation != null){
            insertedConsumerConnectionAreaInformation = consumerConnectionAreaInformationRepository.save(consumerConnectionAreaInformation);
        }
        return insertedConsumerConnectionAreaInformation;
    }

    @Override
    public ConsumerConnectionAreaInformationInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation = null;
        consumerConnectionAreaInformation = consumerConnectionAreaInformationRepository.findOne(id);
        return consumerConnectionAreaInformation;
    }

    @Override
    public ConsumerConnectionAreaInformationInterface get(ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ConsumerConnectionAreaInformationInterface existingConsumerConnectionAreaInformation = null;
        if(consumerConnectionAreaInformation != null){
            existingConsumerConnectionAreaInformation = consumerConnectionAreaInformationRepository.findOne(consumerConnectionAreaInformation.getId());
        }
        return existingConsumerConnectionAreaInformation;
    }

    @Override
    public ConsumerConnectionAreaInformationInterface update(ConsumerConnectionAreaInformationInterface consumerConnectionAreaInformation) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ConsumerConnectionAreaInformationInterface updatedConsumerConnectionAreaInformation = null;
        if(consumerConnectionAreaInformation != null){
            updatedConsumerConnectionAreaInformation = consumerConnectionAreaInformationRepository.save(consumerConnectionAreaInformation);
        }
        return updatedConsumerConnectionAreaInformation;
    }
}
