package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerConnectionInformationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerConnectionInformationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
@Service
public class ConsumerConnectionInformationDAO implements ConsumerConnectionInformationDAOInterface<ConsumerConnectionInformationInterface> {
    @Autowired
    private ConsumerConnectionInformationRepository consumerConnectionInformationRepository;
    /**
     *
     */
    private Logger logger = GlobalResources.getLogger(ConsumerConnectionInformationDAO.class);

    @Override
    public ConsumerConnectionInformationInterface getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        if(consumerNo != null){
            consumerConnectionInformation = consumerConnectionInformationRepository.findByConsumerNo(consumerNo);
        }
        return consumerConnectionInformation;
    }

    @Override
    public List<ConsumerConnectionInformationInterface> getByIsCapacitorSurcharge(String isCapacitorSurcharge) {
        final String methodName = "getByIsCapacitorSurcharge() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationInterface> consumerConnectionInformations = null;
        if(isCapacitorSurcharge != null){
            consumerConnectionInformations = consumerConnectionInformationRepository.findByIsCapacitorSurcharge(isCapacitorSurcharge);
        }
        return consumerConnectionInformations;
    }

    @Override
    public List<ConsumerConnectionInformationInterface> getByIsDemandside(String isDemandSide) {
        final String methodName = "getByIsDemandside() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationInterface> consumerConnectionInformations = null;
        if(isDemandSide != null){
            consumerConnectionInformations = consumerConnectionInformationRepository.findByIsDemandside(isDemandSide);
        }
        return consumerConnectionInformations;
    }

    @Override
    public List<ConsumerConnectionInformationInterface> getByIsGovernment(String isGovernment) {
        final String methodName = "getByIsGovernment() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationInterface> consumerConnectionInformations = null;
        if(isGovernment != null){
            consumerConnectionInformations = consumerConnectionInformationRepository.findByIsGovernment(isGovernment);
        }
        return consumerConnectionInformations;
    }

    @Override
    public List<ConsumerConnectionInformationInterface> getByIsWeldingTransformerSurcharge(String isWeldingTransformerSurcharge) {
        final String methodName = "getByIsWeldingTransformerSurcharge() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationInterface> consumerConnectionInformations = null;
        if(isWeldingTransformerSurcharge != null){
            consumerConnectionInformations = consumerConnectionInformationRepository.findByIsWeldingTransformerSurcharge(isWeldingTransformerSurcharge);
        }
        return consumerConnectionInformations;
    }

    @Override
    public List<ConsumerConnectionInformationInterface> getByIsXray(String isXray) {
        final String methodName = "getByIsXray() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationInterface> consumerConnectionInformations = null;
        if(isXray != null){
            consumerConnectionInformations = consumerConnectionInformationRepository.findByIsXray(isXray);
        }
        return consumerConnectionInformations;
    }

    @Override
    public List<ConsumerConnectionInformationInterface> getBySubCategoryCode(Long subCategoryCode) {
        final String methodName = "getBySubCategoryCode() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationInterface> consumerConnectionInformations = null;
        if(subCategoryCode != null){
            consumerConnectionInformations = consumerConnectionInformationRepository.findBySubCategoryCode(subCategoryCode);
        }
        return consumerConnectionInformations;
    }

    @Override
    public List<ConsumerConnectionInformationInterface> getByTariffCategory(String tariffCategory) {
        final String methodName = "getByTariffCategory() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationInterface> consumerConnectionInformations = null;
        if(tariffCategory != null){
            consumerConnectionInformations = consumerConnectionInformationRepository.findByTariffCategory(tariffCategory);
        }
        return consumerConnectionInformations;
    }

    @Override
    public List<ConsumerConnectionInformationInterface> getByTariffCategoryAndConnectionType(String tariffCategory, String connectionType) {
        final String methodName = "getByTariffCategoryAndConnectionType() : ";
        logger.info(methodName + "called");
        List<ConsumerConnectionInformationInterface> consumerConnectionInformations = null;
        if(tariffCategory != null && connectionType != null){
            consumerConnectionInformations = consumerConnectionInformationRepository.findByTariffCategoryAndConnectionType(tariffCategory, connectionType);
        }
        return consumerConnectionInformations;
    }

    @Override
    public ConsumerConnectionInformationInterface add(ConsumerConnectionInformationInterface consumerConnectionInformation) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationInterface insertedConsumerConnectionInformation = null;
        if(consumerConnectionInformation != null){
            insertedConsumerConnectionInformation = consumerConnectionInformationRepository.save(consumerConnectionInformation);
        }
        return insertedConsumerConnectionInformation;
    }

    @Override
    public ConsumerConnectionInformationInterface get(ConsumerConnectionInformationInterface consumerConnectionInformation) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationInterface existingConsumerConnectionInformation = null;
        if(consumerConnectionInformation != null){
            existingConsumerConnectionInformation = consumerConnectionInformationRepository.findOne(consumerConnectionInformation.getId());
        }
        return existingConsumerConnectionInformation;
    }

    @Override
    public ConsumerConnectionInformationInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationInterface consumerConnectionInformation = null;
        consumerConnectionInformation = consumerConnectionInformationRepository.findOne(id);
        return consumerConnectionInformation;
    }

    @Override
    public ConsumerConnectionInformationInterface update(ConsumerConnectionInformationInterface consumerConnectionInformation) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ConsumerConnectionInformationInterface updatedConsumerConnectionInformation = null;
        if(consumerConnectionInformation != null){
            updatedConsumerConnectionInformation = consumerConnectionInformationRepository.save(consumerConnectionInformation);
        }
        return updatedConsumerConnectionInformation;
    }
}
