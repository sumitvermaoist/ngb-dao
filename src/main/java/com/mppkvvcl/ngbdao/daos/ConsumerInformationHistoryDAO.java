package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerInformationHistoryDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerInformationHistoryRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerInformationHistoryInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by SHIVANSHU on 15-09-2017.
 */
@Service
public class ConsumerInformationHistoryDAO implements ConsumerInformationHistoryDAOInterface<ConsumerInformationHistoryInterface> {

    Logger logger = GlobalResources.getLogger(ConsumerInformationHistoryDAO.class);

    @Autowired
    ConsumerInformationHistoryRepository consumerInformationHistoryRepository;

    @Override
    public ConsumerInformationHistoryInterface add(ConsumerInformationHistoryInterface consumerInformationHistory) {
        final String methodName = "add() : ";
        logger.info(methodName+"Dao got request for add");
        ConsumerInformationHistoryInterface insertedConsumerInformationHistory = null;
        if (consumerInformationHistory != null){
            insertedConsumerInformationHistory = consumerInformationHistoryRepository.save(consumerInformationHistory);
        }
        return insertedConsumerInformationHistory;
    }

    @Override
    public ConsumerInformationHistoryInterface update(ConsumerInformationHistoryInterface consumerInformationHistory) {
        final String methodName = "update() : ";
        logger.info(methodName+"Dao got request for update");
        ConsumerInformationHistoryInterface newConsumerInformationHistory = null;
        if (consumerInformationHistory != null){
            newConsumerInformationHistory = consumerInformationHistoryRepository.save(consumerInformationHistory);
        }
        return newConsumerInformationHistory;
    }

    @Override
    public ConsumerInformationHistoryInterface get(ConsumerInformationHistoryInterface consumerInformationHistory) {
        final String methodName = "get() : ";
        logger.info(methodName+"Dao got request for get");
        ConsumerInformationHistoryInterface oldConsumerInformationHistory = null;
        if (consumerInformationHistory != null){
            oldConsumerInformationHistory = consumerInformationHistoryRepository.findOne(consumerInformationHistory.getId());
        }
        return oldConsumerInformationHistory;
    }

    @Override
    public ConsumerInformationHistoryInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName+"Dao got request for get by Id");
        ConsumerInformationHistoryInterface oldConsumerInformationHistory = null;
        if (id > 0){
            oldConsumerInformationHistory = consumerInformationHistoryRepository.findOne(id);
        }
        return oldConsumerInformationHistory;
    }

    @Override
    public List<ConsumerInformationHistoryInterface> getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() :  ";
        logger.info(methodName+"Dao called to get ConsumerInformationHistory list by consumerNo");
        List<ConsumerInformationHistoryInterface> consumerInformationHistories = null;
        if (consumerNo != null){
            consumerInformationHistories = consumerInformationHistoryRepository.findByConsumerNo(consumerNo);
        }
        return consumerInformationHistories;
    }

    @Override
    public List<ConsumerInformationHistoryInterface> getByEndDateBetween(Date startDate, Date endingDate) {
        final String methodName = "getByEndDateBetween() :  ";
        logger.info(methodName+"Dao called to get ConsumerInformationHistory list by end date between starting date and ending date");
        List<ConsumerInformationHistoryInterface> consumerInformationHistories = null;
        if (startDate != null && endingDate != null){
            consumerInformationHistories = consumerInformationHistoryRepository.findByEndDateBetween(startDate , endingDate);
        }
        return consumerInformationHistories;
    }

    @Override
    public List<ConsumerInformationHistoryInterface> getByPropertyNameAndEndDateBetween(String propertyName, Date startingDate, Date endingDate) {
        final String methodName = "getByPropertyNameAndEndDateBetween() :  ";
        logger.info(methodName+"Dao called to get ConsumerInformationHistory list by property Name and end date between starting date and ending date ");
        List<ConsumerInformationHistoryInterface> consumerInformationHistories = null;
        if(propertyName != null && startingDate != null && endingDate != null){
            consumerInformationHistories = consumerInformationHistoryRepository.findByPropertyNameAndEndDateBetween(propertyName , startingDate , endingDate);
        }
        return consumerInformationHistories;
    }
}
