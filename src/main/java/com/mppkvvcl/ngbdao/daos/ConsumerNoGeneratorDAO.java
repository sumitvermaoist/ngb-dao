package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerNoGeneratorDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerNoGeneratorRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerNoGeneratorInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class ConsumerNoGeneratorDAO implements ConsumerNoGeneratorDAOInterface<ConsumerNoGeneratorInterface> {

    Logger logger = GlobalResources.getLogger(ConsumerNoGeneratorDAO.class);

    @Autowired
    ConsumerNoGeneratorRepository consumerNoGeneratorRepository;

    @Override
    public ConsumerNoGeneratorInterface add(ConsumerNoGeneratorInterface consumerNoGenerator) {
        final String methodName  = "add() :  ";
        logger.info(methodName +"clicked");
        ConsumerNoGeneratorInterface insertedConsumerNoGenerator = null;
        if (consumerNoGenerator != null){
            insertedConsumerNoGenerator = consumerNoGeneratorRepository.save(consumerNoGenerator);
        }
        return insertedConsumerNoGenerator;
    }

    @Override
    public ConsumerNoGeneratorInterface update(ConsumerNoGeneratorInterface consumerNoGenerator) {
        final String methodName  = "update() :  ";
        logger.info(methodName +"clicked");
        ConsumerNoGeneratorInterface updateConsumerNoGenerator = null;
        if (consumerNoGenerator != null){
            updateConsumerNoGenerator = consumerNoGeneratorRepository.save(consumerNoGenerator);
        }
        return updateConsumerNoGenerator;
    }

    @Override
    public ConsumerNoGeneratorInterface get(ConsumerNoGeneratorInterface consumerNoGenerator) {
        final String methodName  = "get() :  ";
        logger.info(methodName +"clicked");
        ConsumerNoGeneratorInterface getConsumerNoGenerator = null;
        if (consumerNoGenerator != null){
            getConsumerNoGenerator = consumerNoGeneratorRepository.findOne(consumerNoGenerator.getId());
        }
        return getConsumerNoGenerator;
    }

    @Override
    public ConsumerNoGeneratorInterface getById(long id) {
        final String methodName  = "getById() :  ";
        logger.info(methodName +"clicked");
        ConsumerNoGeneratorInterface consumerNoGenerator = null;
        if (id > 0){
            consumerNoGenerator = consumerNoGeneratorRepository.getOne(id);
        }
        return consumerNoGenerator;
    }

    @Override
    public ConsumerNoGeneratorInterface getByLocationCode(String locationCode) {
        final String methodName  = "getByLocationCode() :  ";
        logger.info(methodName+"clicked");
        ConsumerNoGeneratorInterface consumerNoGenerator =  null;
        if (locationCode != null){
            consumerNoGenerator = consumerNoGeneratorRepository.findByLocationCode(locationCode);
        }
        return consumerNoGenerator;
    }
}
