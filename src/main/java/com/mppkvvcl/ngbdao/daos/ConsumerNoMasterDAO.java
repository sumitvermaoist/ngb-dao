package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ConsumerNoMasterDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ConsumerNoMasterRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ConsumerNoMasterInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by SHIVANSHU on 16-09-2017.
 * updated by Sumit Verma
 */
@Service
public class ConsumerNoMasterDAO implements ConsumerNoMasterDAOInterface<ConsumerNoMasterInterface> {

    private Logger logger = GlobalResources.getLogger(ConsumerNoMasterDAO.class);

    @Autowired
    private ConsumerNoMasterRepository consumerNoMasterRepository;


    @Override
    public ConsumerNoMasterInterface add(ConsumerNoMasterInterface consumerNoMaster) {
        final String methodName  = "add() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface insertedConsumerNoMaster= null;
        if (consumerNoMaster != null){
            insertedConsumerNoMaster = consumerNoMasterRepository.save(consumerNoMaster);
        }
        return insertedConsumerNoMaster;
    }

    @Override
    public ConsumerNoMasterInterface update(ConsumerNoMasterInterface consumerNoMaster) {
        final String methodName  = "update() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface updatedConsumerNoMaster= null;
        if (consumerNoMaster != null){
            updatedConsumerNoMaster = consumerNoMasterRepository.save(consumerNoMaster);
        }
        return updatedConsumerNoMaster;
    }

    @Override
    public ConsumerNoMasterInterface get(ConsumerNoMasterInterface consumerNoMaster) {
        final String methodName  = "get() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface getConsumerNoMaster= null;
        if (consumerNoMaster != null){
            getConsumerNoMaster = consumerNoMasterRepository.findOne(consumerNoMaster.getId());
        }
        return getConsumerNoMaster;
    }

    @Override
    public ConsumerNoMasterInterface getById(long id) {
        final String methodName  = "getById() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface getConsumerNoMaster= null;
        if (id > 0){
            getConsumerNoMaster = consumerNoMasterRepository.getOne(id);
        }
        return getConsumerNoMaster;
    }

    @Override
    public ConsumerNoMasterInterface getByOldServiceNoOne(String oldServiceNoOne) {
        final String methodName  = "getByOldServiceNoOne() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface consumerNoMaster= null;
        if (oldServiceNoOne != null){
            consumerNoMaster = consumerNoMasterRepository.findByOldServiceNoOne(oldServiceNoOne);
        }
        return consumerNoMaster;
    }

    @Override
    public ConsumerNoMasterInterface getByOldServiceNoOneAndLocationCode(String oldServiceNoOne, String locationCode) {
        final String methodName  = "getByOldServiceNoOneAndLocationCode() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface consumerNoMaster= null;
        if (oldServiceNoOne != null){
            consumerNoMaster = consumerNoMasterRepository.findByOldServiceNoOneAndLocationCode(oldServiceNoOne, locationCode);
        }
        return consumerNoMaster;
    }

    @Override
    public ConsumerNoMasterInterface getByOldServiceNoTwo(String oldServiceNoTwo) {
        final String methodName  = "getByOldServiceNoTwo() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface consumerNoMaster= null;
        if (oldServiceNoTwo != null){
            consumerNoMaster = consumerNoMasterRepository.findByOldServiceNoOne(oldServiceNoTwo);
        }
        return consumerNoMaster;
    }

    @Override
    public ConsumerNoMasterInterface getByConsumerNo(String consumerNo) {
        final String methodName  = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        ConsumerNoMasterInterface consumerNoMaster= null;
        if (consumerNo != null){
            consumerNoMaster = consumerNoMasterRepository.findByConsumerNo(consumerNo);
        }
        return consumerNoMaster;
    }

    @Override
    public long countByGroupNo(String groupNo) {
        final String methodName  = "countByGroupNo() : ";
        logger.info(methodName + "called");
        long count = 0;
        if (groupNo != null){
            count = consumerNoMasterRepository.countByGroupNo(groupNo);
        }
        return count;
    }

    @Override
    public long countByGroupNoAndReadingDiaryNo(String groupNo, String readingDiaryNo) {
        final String methodName = "countByGroupNoAndReadingDiaryNo() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null && readingDiaryNo != null){
             count = consumerNoMasterRepository.countByGroupNoAndReadingDiaryNo(groupNo,readingDiaryNo);
        }
        return count;
    }

    @Override
    public long countByGroupNoAndReadingDiaryNoAndStatus(String groupNo, String readingDiaryNo, String status) {
        final String methodName = "countByGroupNoAndReadingDiaryNoAndStatus() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null && readingDiaryNo != null && status != null){
             count = consumerNoMasterRepository.countByGroupNoAndReadingDiaryNoAndStatus(groupNo,readingDiaryNo,status);
        }
        return count;
    }

    @Override
    public long countByGroupNoAndStatus(String groupNo, String status) {
        final String methodName = "countByGroupNoAndStatus() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(groupNo != null && status != null){
            count = consumerNoMasterRepository.countByGroupNoAndStatus(groupNo,status);
        }
        return count;
    }

    @Override
    public long countByLocationCode(String locationCode) {
        final String methodName = "countByLocationCode() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(locationCode != null){
            count = consumerNoMasterRepository.countByLocationCode(locationCode);
        }
        return count;
    }

    @Override
    public long countByLocationCodeAndStatus(String locationCode, String status) {
        final String methodName = "countByLocationCodeAndStatus() : ";
        logger.info(methodName + "called");
        long count = 0;
        if(locationCode != null && status != null){
            count = consumerNoMasterRepository.countByLocationCodeAndStatus(locationCode,status);
        }
        return count;
    }
}
