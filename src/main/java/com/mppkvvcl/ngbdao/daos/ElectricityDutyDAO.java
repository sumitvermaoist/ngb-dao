package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.ElectricityDutyDAOInterface;
import com.mppkvvcl.ngbdao.repositories.ElectricityDutyRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.ElectricityDutyInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class ElectricityDutyDAO implements ElectricityDutyDAOInterface<ElectricityDutyInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(ElectricityDutyDAO.class);

    @Autowired
    ElectricityDutyRepository electricityDutyRepository = null;

    @Override
    public ElectricityDutyInterface add(ElectricityDutyInterface electricityDuty) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        ElectricityDutyInterface insertedElectricityDuty = null;
        if (electricityDuty != null){
            insertedElectricityDuty = electricityDutyRepository.save(electricityDuty);
        }
        return insertedElectricityDuty;
    }

    @Override
    public ElectricityDutyInterface update(ElectricityDutyInterface electricityDuty) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        ElectricityDutyInterface updatedElectricityDuty = null;
        if (electricityDuty != null){
            updatedElectricityDuty = electricityDutyRepository.save(electricityDuty);
        }
        return updatedElectricityDuty;
    }

    @Override
    public ElectricityDutyInterface get(ElectricityDutyInterface electricityDuty) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        ElectricityDutyInterface existingElectricityDuty = null;
        if (electricityDuty != null){
            existingElectricityDuty = electricityDutyRepository.findOne(electricityDuty.getId());
        }
        return existingElectricityDuty;
    }

    @Override
    public ElectricityDutyInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        ElectricityDutyInterface existingElectricityDuty = null;
        if(id != 0){
            existingElectricityDuty = electricityDutyRepository.findOne(id);
        }
        return existingElectricityDuty;
    }

    @Override
    public List<ElectricityDutyInterface> getBySubCategoryCodeAndDate(Long subCategoryCode, Date givenDate) {
        final String methodName = "getBySubCategoryCodeAndDate() : ";
        logger.info(methodName + "called");
        List<ElectricityDutyInterface> fetchedElectricityDuty = null;
        if (subCategoryCode != null && givenDate != null) {
            fetchedElectricityDuty = electricityDutyRepository.findBySubCategoryCodeAndDate(subCategoryCode,givenDate);
        }
        return fetchedElectricityDuty;
    }
}
