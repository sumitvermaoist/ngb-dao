package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.GMCAccountingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.GMCAccountingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.GMCAccountingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by PREETESH on 11/7/2017.
 */

@Service
public class GMCAccountingDAO implements GMCAccountingDAOInterface<GMCAccountingInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(GMCAccountingDAO.class);

    @Autowired
    private GMCAccountingRepository gmcAccountingRepository;


    @Override
    public GMCAccountingInterface add(GMCAccountingInterface gmcAccountingInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        GMCAccountingInterface insertedGMCAccounting = null;
        if (gmcAccountingInterface != null){
            insertedGMCAccounting = gmcAccountingRepository.save(gmcAccountingInterface);
        }
        return insertedGMCAccounting;
    }

    @Override
    public GMCAccountingInterface update(GMCAccountingInterface gmcAccounting) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        GMCAccountingInterface updatedGMCAccounting = null;
        if (gmcAccounting != null){
            updatedGMCAccounting = gmcAccountingRepository.save(gmcAccounting);
        }
        return updatedGMCAccounting;
    }

    @Override
    public GMCAccountingInterface get(GMCAccountingInterface gmcAccounting) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        GMCAccountingInterface existingGMCAccounting = null;
        if (gmcAccounting != null){
            existingGMCAccounting = gmcAccountingRepository.findOne(gmcAccounting.getId());
        }
        return existingGMCAccounting;
    }

    @Override
    public GMCAccountingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        GMCAccountingInterface existingGMCAccounting = null;
        if(id != 0){
            existingGMCAccounting = gmcAccountingRepository.findOne(id);
        }
        return existingGMCAccounting;
    }

    @Override
    public GMCAccountingInterface getByConsumerNo(String consumerNo) {
        final String methodName  = "getByConsumerNo() : ";
        logger.info(methodName + "called");
        GMCAccountingInterface gmcAccounting= null;
        if (consumerNo != null){
            gmcAccounting = gmcAccountingRepository.findByConsumerNo(consumerNo);
        }
        return gmcAccounting;
    }

    @Override
    public GMCAccountingInterface getByConsumerNoAndCurrentBillMonth(String consumerNo, String currentBillMonth) {
        final String methodName  = "getByConsumerNoAndCurrentBillMonth() : ";
        logger.info(methodName + "called");
        GMCAccountingInterface gmcAccounting= null;
        if (consumerNo != null){
            gmcAccounting = gmcAccountingRepository.findByConsumerNoAndCurrentBillMonth(consumerNo, currentBillMonth);
        }
        return gmcAccounting;
    }
}