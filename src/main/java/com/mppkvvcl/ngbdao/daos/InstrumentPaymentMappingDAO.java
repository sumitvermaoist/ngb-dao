package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.InstrumentPaymentMappingDAOInterface;
import com.mppkvvcl.ngbdao.repositories.InstrumentPaymentMappingRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.InstrumentPaymentMappingInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class InstrumentPaymentMappingDAO implements InstrumentPaymentMappingDAOInterface<InstrumentPaymentMappingInterface> {


    /**
     * To get Logger object for logging in current class.
     */
    Logger logger = GlobalResources.getLogger(InstrumentPaymentMappingDAO.class);

    @Autowired
    InstrumentPaymentMappingRepository instrumentPaymentMappingRepository = null;


    @Override
    public InstrumentPaymentMappingInterface add(InstrumentPaymentMappingInterface instrumentPaymentMapping) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        InstrumentPaymentMappingInterface insertedInstrumentPaymentMapping = null;
        if (instrumentPaymentMapping != null){
            insertedInstrumentPaymentMapping = instrumentPaymentMappingRepository.save(instrumentPaymentMapping);
        }
        return insertedInstrumentPaymentMapping;
    }

    @Override
    public InstrumentPaymentMappingInterface update(InstrumentPaymentMappingInterface instrumentPaymentMapping) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        InstrumentPaymentMappingInterface updatedInstrumentPaymentMapping = null;
        if (instrumentPaymentMapping != null){
            updatedInstrumentPaymentMapping = instrumentPaymentMappingRepository.save(instrumentPaymentMapping);
        }
        return updatedInstrumentPaymentMapping;
    }

    @Override
    public InstrumentPaymentMappingInterface get(InstrumentPaymentMappingInterface instrumentPaymentMapping) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        InstrumentPaymentMappingInterface existingInstrumentPaymentMapping = null;
        if (instrumentPaymentMapping != null){
            existingInstrumentPaymentMapping = instrumentPaymentMappingRepository.findOne(instrumentPaymentMapping.getId());
        }
        return existingInstrumentPaymentMapping;
    }

    @Override
    public InstrumentPaymentMappingInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        InstrumentPaymentMappingInterface existingInstrumentPaymentMapping = null;
        if(id != 0){
            existingInstrumentPaymentMapping = instrumentPaymentMappingRepository.findOne(id);
        }
        return existingInstrumentPaymentMapping;
    }

    @Override
    public List<InstrumentPaymentMappingInterface> getByInstrumentDetailIdOrderByPaymentIdAsc(long instrumentDetailId) {
        final String methodName = "getByInstrumentDetailIdOrderByPaymentIdAsc() : ";
        logger.info(methodName + "called");
        List<InstrumentPaymentMappingInterface> existingInstrumentPaymentMapping = null;
        if(instrumentDetailId != 0){
            existingInstrumentPaymentMapping = instrumentPaymentMappingRepository.findByInstrumentDetailIdOrderByPaymentIdAsc(instrumentDetailId);
        }
        return existingInstrumentPaymentMapping;
    }

    @Override
    public InstrumentPaymentMappingInterface getByPaymentId(long id) {
        final String methodName = "getByPaymentId() : ";
        logger.info(methodName + "called");
        InstrumentPaymentMappingInterface existingInstrumentPaymentMapping = null;
        if(id != 0){
            existingInstrumentPaymentMapping = instrumentPaymentMappingRepository.findByPaymentId(id);
        }
        return existingInstrumentPaymentMapping;
    }

    @Override
    public void delete(InstrumentPaymentMappingInterface instrumentPaymentMappingInterface) {
        final String methodName = "deleteById() : ";
        logger.info(methodName + "called");
        if(instrumentPaymentMappingInterface != null){
             instrumentPaymentMappingRepository.delete(instrumentPaymentMappingInterface);
        }
    }
}
