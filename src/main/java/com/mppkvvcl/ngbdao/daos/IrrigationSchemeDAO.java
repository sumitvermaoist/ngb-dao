package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.IrrigationSchemeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.IrrigationSchemeRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.IrrigationSchemeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IrrigationSchemeDAO implements IrrigationSchemeDAOInterface<IrrigationSchemeInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(IrrigationSchemeDAO.class);

    /**
     * Requesting spring to get singleton AdjustmentRepository object.
     */
    @Autowired
    IrrigationSchemeRepository irrigationSchemeRepository;


    @Override
    public IrrigationSchemeInterface add(IrrigationSchemeInterface irrigationScheme) {
        final String methodName = "add() : ";
        IrrigationSchemeInterface insertedIrrigationScheme =null;
        if(irrigationScheme != null){
            insertedIrrigationScheme = irrigationSchemeRepository.save(irrigationScheme);
        }
        return insertedIrrigationScheme;
    }

    @Override
    public IrrigationSchemeInterface update(IrrigationSchemeInterface irrigationScheme) {
        final String methodName = "update() : ";
        IrrigationSchemeInterface updatedIrrigationScheme =null;
        if(irrigationScheme != null){
            updatedIrrigationScheme = irrigationSchemeRepository.save(irrigationScheme);
        }
        return updatedIrrigationScheme;
    }

    @Override
    public IrrigationSchemeInterface get(IrrigationSchemeInterface irrigationScheme) {
        final String methodName = "get() : ";
        IrrigationSchemeInterface fetchedIrrigationScheme =null;
        if(irrigationScheme != null){
            fetchedIrrigationScheme = irrigationSchemeRepository.findOne(irrigationScheme.getId());
        }
        return fetchedIrrigationScheme;
    }

    @Override
    public IrrigationSchemeInterface getById(long id) {
        final String methodName = "getById() : ";
        IrrigationSchemeInterface irrigationScheme =null;
        irrigationScheme = irrigationSchemeRepository.findOne(id);
        return irrigationScheme;
    }

    @Override
    public IrrigationSchemeInterface getByConsumerNo(String consumerNo) {
        final String methodName = "getByConsumerNo() : ";
        IrrigationSchemeInterface irrigationScheme =null;
        if(consumerNo != null){
            irrigationScheme = irrigationSchemeRepository.findByConsumerNo(consumerNo);
        }
        return irrigationScheme;

    }

}
