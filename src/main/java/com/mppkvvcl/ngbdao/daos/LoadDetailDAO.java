package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.LoadDetailDAOInterface;
import com.mppkvvcl.ngbdao.repositories.LoadDetailRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.LoadDetailInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
@Service
public class LoadDetailDAO  implements LoadDetailDAOInterface<LoadDetailInterface> {

    Logger logger = GlobalResources.getLogger(LoadDetailDAO.class);

    @Autowired
    LoadDetailRepository loadDetailRepository;

    @Override
    public LoadDetailInterface add(LoadDetailInterface loadDetail) {
        final String methodName = "add() :  ";
        logger.info(methodName + "called");
        LoadDetailInterface insertedLoadDetail  = null;
        if (loadDetail !=  null){
            insertedLoadDetail = loadDetailRepository.save(loadDetail);
        }
        return insertedLoadDetail;
    }

    @Override
    public LoadDetailInterface update(LoadDetailInterface loadDetail) {
        final String methodName = "update() :  ";
        logger.info(methodName + "called");
        LoadDetailInterface updatedLoadDetail  = null;
        if (loadDetail !=  null){
            updatedLoadDetail = loadDetailRepository.save(loadDetail);
        }
        return updatedLoadDetail;
    }

    @Override
    public LoadDetailInterface get(LoadDetailInterface loadDetail) {
        final String methodName = "get() :  ";
        logger.info(methodName + "called");
        LoadDetailInterface getLoadDetail  = null;
        if (loadDetail !=  null){
            getLoadDetail = loadDetailRepository.findOne(loadDetail.getId());
        }
        return getLoadDetail;
    }

    @Override
    public LoadDetailInterface getById(long id) {
        final String methodName = "getById() :  ";
        logger.info(methodName + "called");
        LoadDetailInterface loadDetail  = null;
        if (id > 0){
            loadDetail = loadDetailRepository.findOne(id);
        }
        return loadDetail;
    }

    @Override
    public LoadDetailInterface getTopByConsumerNoOrderByIdDesc(String consumerNo) {
        final String methodName  = "getTopByConsumerNoOrderByIdDesc()  : ";
        logger.info(methodName + "called");
        LoadDetailInterface loadDetail = null;
        if (consumerNo != null){
            loadDetail = loadDetailRepository.findTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return loadDetail;
    }

    @Override
    public List<LoadDetailInterface> getByConsumerNo(final String consumerNo){
        String methodName = "getByConsumerNo()";
        logger.info(methodName + "called for consumer No : " + consumerNo);
        return loadDetailRepository.findByConsumerNoOrderByIdDesc(consumerNo);
    }

    @Override
    public List<LoadDetailInterface> getByConsumerNoAndContractDemandNotNullOrderByIdAsc(String consumerNo) {
        String methodName = "getByConsumerNoAndContractDemandNotNullOrderByIdAsc() : ";
        logger.info(methodName + "called");
        List<LoadDetailInterface> loadDetailInterfaces = null;
        if(consumerNo != null){
            loadDetailInterfaces = loadDetailRepository.findByConsumerNoAndContractDemandNotNullOrderByIdAsc(consumerNo);
        }
        return loadDetailInterfaces;
    }
}
