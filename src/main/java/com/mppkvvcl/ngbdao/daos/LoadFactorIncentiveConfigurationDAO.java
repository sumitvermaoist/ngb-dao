package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.LoadFactorIncentiveConfigurationDAOInterface;
import com.mppkvvcl.ngbdao.repositories.LoadFactorIncentiveConfigurationRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.LoadFactorIncentiveConfigurationInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 12/22/2017.
 */
@Service
public class LoadFactorIncentiveConfigurationDAO implements LoadFactorIncentiveConfigurationDAOInterface<LoadFactorIncentiveConfigurationInterface> {
    /**
     * Requesting spring to get singleton Repository object.
     */
    @Autowired
    LoadFactorIncentiveConfigurationRepository loadFactorIncentiveConfigurationRepository;

    /**
     * Getting whole logger object from global resources for current class.
     */
    Logger logger = GlobalResources.getLogger(LoadFactorIncentiveConfigurationDAO.class);


    @Override
    public LoadFactorIncentiveConfigurationInterface add(LoadFactorIncentiveConfigurationInterface loadFactorIncentiveConfigurationInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        LoadFactorIncentiveConfigurationInterface insertedLoadFactorIncentiveConfigurationInterface = null;
        if(loadFactorIncentiveConfigurationInterface != null){
            insertedLoadFactorIncentiveConfigurationInterface = loadFactorIncentiveConfigurationRepository.save(loadFactorIncentiveConfigurationInterface);
        }
        return insertedLoadFactorIncentiveConfigurationInterface;
    }

    @Override
    public LoadFactorIncentiveConfigurationInterface update(LoadFactorIncentiveConfigurationInterface loadFactorIncentiveConfigurationInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        LoadFactorIncentiveConfigurationInterface updatedLoadFactorIncentiveConfigurationInterface = null;
        if(loadFactorIncentiveConfigurationInterface != null){
            updatedLoadFactorIncentiveConfigurationInterface = loadFactorIncentiveConfigurationRepository.save(loadFactorIncentiveConfigurationInterface);
        }
        return updatedLoadFactorIncentiveConfigurationInterface;
    }

    @Override
    public LoadFactorIncentiveConfigurationInterface get(LoadFactorIncentiveConfigurationInterface loadFactorIncentiveConfigurationInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        LoadFactorIncentiveConfigurationInterface fetchedLoadFactorIncentiveConfigurationInterface = null;
        if(loadFactorIncentiveConfigurationInterface != null){
            fetchedLoadFactorIncentiveConfigurationInterface = loadFactorIncentiveConfigurationRepository.findOne(loadFactorIncentiveConfigurationInterface.getId());
        }
        return fetchedLoadFactorIncentiveConfigurationInterface;
    }

    @Override
    public LoadFactorIncentiveConfigurationInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        LoadFactorIncentiveConfigurationInterface loadFactorIncentiveConfigurationInterface = null;
        loadFactorIncentiveConfigurationInterface = loadFactorIncentiveConfigurationRepository.findOne(id);
        return loadFactorIncentiveConfigurationInterface;
    }


    @Override
    public LoadFactorIncentiveConfigurationInterface getByLoadFactorValueAndDate(BigDecimal lfValue, Date date) {
        final String methodName = "getByLoadFactorValueAndDate() : ";
        logger.info(methodName + "called");
        LoadFactorIncentiveConfigurationInterface loadFactorIncentiveConfigurationInterface = null;
        if(lfValue != null && date != null){
            loadFactorIncentiveConfigurationInterface = loadFactorIncentiveConfigurationRepository.findByLoadFactorValueAndDate(lfValue,date);
        }
        return loadFactorIncentiveConfigurationInterface;
    }

    @Override
    public List<LoadFactorIncentiveConfigurationInterface> getByDate(Date date) {
        final String methodName = "getByDate() : ";
        logger.info(methodName + "called");
        List<LoadFactorIncentiveConfigurationInterface> loadFactorIncentiveConfigurationInterfaces = null;
        if(date != null){
            loadFactorIncentiveConfigurationInterfaces = loadFactorIncentiveConfigurationRepository.findByDate(date);
        }
        return loadFactorIncentiveConfigurationInterfaces;
    }
}
