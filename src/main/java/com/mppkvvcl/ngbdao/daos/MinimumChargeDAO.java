package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.MinimumChargeDAOInterface;
import com.mppkvvcl.ngbdao.repositories.MinimumChargeRepository;
import com.mppkvvcl.ngbdao.repositories.SlabRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.MinimumChargeInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MinimumChargeDAO implements MinimumChargeDAOInterface<MinimumChargeInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(MinimumChargeDAO.class);

    @Autowired
    private MinimumChargeRepository minimumChargeRepository;

    @Override
    public MinimumChargeInterface add(MinimumChargeInterface minimumChargeInterface) {
        return null;
    }

    @Override
    public MinimumChargeInterface update(MinimumChargeInterface minimumChargeInterface) {
        return null;
    }

    @Override
    public MinimumChargeInterface get(MinimumChargeInterface minimumChargeInterface) {
        return null;
    }

    @Override
    public MinimumChargeInterface getById(long id) {
        return null;
    }

    @Override
    public MinimumChargeInterface getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode) {
        final String methodName = "getByTariffIdAndSubcategoryCode() : ";
        logger.info(methodName + "called");
        return minimumChargeRepository.findByTariffIdAndSubcategoryCode(tariffId,subcategoryCode);
    }
}
