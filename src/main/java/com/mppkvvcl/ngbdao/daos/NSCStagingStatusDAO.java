package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.NSCStagingStatusDAOInterface;
import com.mppkvvcl.ngbdao.repositories.NSCStagingStatusRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingStatusInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by nitish on 21-12-2017.
 */
@Service
public class NSCStagingStatusDAO implements NSCStagingStatusDAOInterface<NSCStagingStatusInterface> {

    /**
     * Getting whole logger object from global resources for current class.
     */
    private Logger logger = GlobalResources.getLogger(NSCStagingStatusDAO.class);

    /**
     * Requesting spring to get singleton  NSCStagingStatusRepository object.
     */
    @Autowired
    private NSCStagingStatusRepository nscStagingStatusRepository;

    //count methods
    @Override
    public long getCountByLocationCode(String locationCode) {
        final String methodName = "getCountByLocationCode() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(locationCode != null){
            count = nscStagingStatusRepository.countByLocationCode(locationCode);
        }
        return count;
    }

    @Override
    public long getCountByLocationCodeAndStatus(String locationCode,String status) {
        final String methodName = "getCountByLocationCodeAndStatus() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(locationCode != null && status != null){
            count = nscStagingStatusRepository.countByLocationCodeAndStatus(locationCode,status);
        }
        return count;
    }

    @Override
    public long getCountByGroupNo(String groupNo) {
        final String methodName = "getCountByGroupNo() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(groupNo != null){
            count = nscStagingStatusRepository.countByGroupNo(groupNo);
        }
        return count;
    }

    @Override
    public long getCountByGroupNoAndStatus(String groupNo,String status) {
        final String methodName = "getCountByGroupNoAndStatus() : ";
        logger.info(methodName + "called");
        long count = -1;
        if(groupNo != null && status != null){
            count = nscStagingStatusRepository.countByGroupNoAndStatus(groupNo,status);
        }
        return count;
    }

    //Pageable methods
    @Override
    public List<NSCStagingStatusInterface> getByLocationCode(String locationCode, Pageable pageable) {
        final String methodName = "getByLocationCode() : ";
        logger.info(methodName + "called with pageable ");
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if(locationCode != null){
            nscStagingStatusInterfaces = nscStagingStatusRepository.findByLocationCode(locationCode,pageable);
        }
        return nscStagingStatusInterfaces;
    }

    @Override
    public List<NSCStagingStatusInterface> getByLocationCodeAndStatus(String locationCode,String status, Pageable pageable) {
        final String methodName = "getByLocationCodeAndStatus() : ";
        logger.info(methodName + "called with pageable ");
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if(locationCode != null){
            nscStagingStatusInterfaces = nscStagingStatusRepository.findByLocationCodeAndStatus(locationCode,status,pageable);
        }
        return nscStagingStatusInterfaces;
    }

    @Override
    public List<NSCStagingStatusInterface> getByGroupNo(String groupNo, Pageable pageable) {
        final String methodName = "getByGroupNo() : ";
        logger.info(methodName + "called with pageable ");
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if(groupNo != null){
            nscStagingStatusInterfaces = nscStagingStatusRepository.findByGroupNo(groupNo,pageable);
        }
        return nscStagingStatusInterfaces;
    }

    @Override
    public List<NSCStagingStatusInterface> getByGroupNoAndStatus(String groupNo,String status, Pageable pageable) {
        final String methodName = "getByGroupNoAndStatus() : ";
        logger.info(methodName + "called with pageable ");
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if(groupNo != null){
            nscStagingStatusInterfaces = nscStagingStatusRepository.findByGroupNoAndStatus(groupNo,status,pageable);
        }
        return nscStagingStatusInterfaces;
    }

    //Non-pageable methods
    @Override
    public List<NSCStagingStatusInterface> getByLocationCode(String locationCode) {
        final String methodName = "getByLocationCode() : ";
        logger.info(methodName + "called for " + locationCode);
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if(locationCode != null){
            nscStagingStatusInterfaces = nscStagingStatusRepository.findByLocationCode(locationCode);
        }
        return nscStagingStatusInterfaces;
    }

    @Override
    public List<NSCStagingStatusInterface> getByLocationCodeAndStatus(String locationCode, String status) {
        final String methodName = "getByLocationCodeAndStatus() : ";
        logger.info(methodName + "called");
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if(locationCode != null && status != null){
            nscStagingStatusInterfaces = nscStagingStatusRepository.findByLocationCodeAndStatus(locationCode,status);
        }
        return nscStagingStatusInterfaces;
    }

    @Override
    public List<NSCStagingStatusInterface> getByGroupNo(String groupNo) {
        final String methodName = "getByGroupNo() : ";
        logger.info(methodName + "called");
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if(groupNo != null){
            nscStagingStatusInterfaces = nscStagingStatusRepository.findByGroupNo(groupNo);
        }
        return nscStagingStatusInterfaces;
    }

    @Override
    public List<NSCStagingStatusInterface> getByGroupNoAndStatus(String groupNo, String status) {
        final String methodName = "getByGroupNoAndStatus() : ";
        logger.info(methodName + "called");
        List<NSCStagingStatusInterface> nscStagingStatusInterfaces = null;
        if(groupNo != null && status != null){
            nscStagingStatusInterfaces = nscStagingStatusRepository.findByGroupNoAndStatus(groupNo,status);
        }
        return nscStagingStatusInterfaces;
    }

    @Override
    public List<NSCStagingStatusInterface> getByStatus(String status) {
        final String methodName = "getByStatus() : ";
        logger.info(methodName + "is called");
        List<NSCStagingStatusInterface> nscStagingStatuses = null;
        if(status != null){
            nscStagingStatuses = nscStagingStatusRepository.findByStatus(status);
        }
        return nscStagingStatuses;
    }

    @Override
    public NSCStagingStatusInterface getByConsumerNo(String consumerNo) {
        final String methodName = "getByGeneratedConsumerNo() : ";
        logger.info(methodName + "is called");
        NSCStagingStatusInterface nscStagingStatus = null;
        if(consumerNo != null){
            nscStagingStatus = nscStagingStatusRepository.findByConsumerNo(consumerNo);
        }
        return nscStagingStatus;
    }

    @Override
    public NSCStagingStatusInterface getByNscStagingId(long nscStagingId) {
        final String methodName = "getByNscStagingId() : ";
        logger.info(methodName + "is called");
        NSCStagingStatusInterface nscStagingStatus = null;
        if(nscStagingId > 0){
            nscStagingStatus = nscStagingStatusRepository.findByNscStagingId(nscStagingId);
        }
        return nscStagingStatus;
    }

    @Override
    public NSCStagingStatusInterface add(NSCStagingStatusInterface nscStagingStatus) {
        final String methodName = "add() : ";
        logger.info(methodName + "is called");
        NSCStagingStatusInterface insertedNSCStagingStatus = null;
        if(nscStagingStatus != null){
            insertedNSCStagingStatus = nscStagingStatusRepository.save(nscStagingStatus);
        }
        return insertedNSCStagingStatus;
    }

    @Override
    public NSCStagingStatusInterface get(NSCStagingStatusInterface nscStagingStatus) {
        final String methodName = "get() : ";
        logger.info(methodName + "is called");
        NSCStagingStatusInterface existingNSCStagingStatus = null;
        if(nscStagingStatus != null){
            existingNSCStagingStatus = nscStagingStatusRepository.findOne(nscStagingStatus.getId());
        }
        return existingNSCStagingStatus;
    }

    @Override
    public NSCStagingStatusInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "is called");
        NSCStagingStatusInterface nscStagingStatus = null;
        nscStagingStatus = nscStagingStatusRepository.findOne(id);
        return nscStagingStatus;
    }

    @Override
    public NSCStagingStatusInterface update(NSCStagingStatusInterface nscStagingStatus) {
        final String methodName = "update() : ";
        logger.info(methodName + "is called");
        NSCStagingStatusInterface updatedNSCStagingStatus = null;
        if(nscStagingStatus != null){
            updatedNSCStagingStatus = nscStagingStatusRepository.save(nscStagingStatus);
        }
        return updatedNSCStagingStatus;
    }

    @Override
    public NSCStagingStatusInterface getByPendingStatusAndMeterIdentifier(String status, String meterIdentifier) {
        final String methodName = "getByPendingStatusAndMeterIdentifier() : ";
        logger.info(methodName + "is called");
        NSCStagingStatusInterface nscStagingStatus = null;
        if(status != null && meterIdentifier != null){
            nscStagingStatus = nscStagingStatusRepository.findByPendingStatusAndMeterIdentifier(status,meterIdentifier);
        }
        return nscStagingStatus;
    }

    @Override
    public NSCStagingStatusInterface getByStatusAndBPlNo(String status, String bplNo) {
        final String methodName = "getByStatusAndBPlNo() : ";
        logger.info(methodName + "called");
        NSCStagingStatusInterface nscStagingStatus = null;
        if(status != null && bplNo != null){
            nscStagingStatus = nscStagingStatusRepository.findByStatusAndBPlNo(status,bplNo);
        }
        return nscStagingStatus;
    }

    @Override
    public NSCStagingStatusInterface getByStatusAndEmployeeNo(String status, String employeeNo) {
        final String methodName = "getByStatusAndEmployeeNo() : ";
        logger.info(methodName + "called");
        NSCStagingStatusInterface nscStagingStatus = null;
        if(status != null && employeeNo != null){
            nscStagingStatus = nscStagingStatusRepository.findByStatusAndEmployeeNo(status,employeeNo);
        }
        return nscStagingStatus;
    }
}
