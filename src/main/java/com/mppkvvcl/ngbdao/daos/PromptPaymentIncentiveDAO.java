package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.PromptPaymentIncentiveDAOInterface;
import com.mppkvvcl.ngbdao.repositories.PromptPaymentIncentiveRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.PromptPaymentIncentiveInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PromptPaymentIncentiveDAO implements PromptPaymentIncentiveDAOInterface<PromptPaymentIncentiveInterface> {

    private Logger logger = GlobalResources.getLogger(PromptPaymentIncentiveDAO.class);

    @Autowired
    private PromptPaymentIncentiveRepository promptPaymentIncentiveRepository;

    @Override
    public PromptPaymentIncentiveInterface getByDate(Date date) {
        final String methodName = "getByDate() : ";
        logger.info(methodName + "called");
        PromptPaymentIncentiveInterface promptPaymentIncentiveInterface = null;
        if(date != null){
            promptPaymentIncentiveInterface = promptPaymentIncentiveRepository.findByDate(date);
        }
        return promptPaymentIncentiveInterface;
    }

    @Override
    public PromptPaymentIncentiveInterface add(PromptPaymentIncentiveInterface promptPaymentIncentiveInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        PromptPaymentIncentiveInterface addedPromptPaymentIncentiveInterface = null;
        if(promptPaymentIncentiveInterface != null){
            addedPromptPaymentIncentiveInterface = promptPaymentIncentiveRepository.save(promptPaymentIncentiveInterface);
        }
        return addedPromptPaymentIncentiveInterface;
    }

    @Override
    public PromptPaymentIncentiveInterface update(PromptPaymentIncentiveInterface promptPaymentIncentiveInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        PromptPaymentIncentiveInterface updatedPromptPaymentIncentiveInterface = null;
        if(promptPaymentIncentiveInterface != null){
            updatedPromptPaymentIncentiveInterface = promptPaymentIncentiveRepository.save(promptPaymentIncentiveInterface);
        }
        return updatedPromptPaymentIncentiveInterface;
    }

    @Override
    public PromptPaymentIncentiveInterface get(PromptPaymentIncentiveInterface promptPaymentIncentiveInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        PromptPaymentIncentiveInterface fetchedPromptPaymentIncentiveInterface = null;
        if(promptPaymentIncentiveInterface != null){
            fetchedPromptPaymentIncentiveInterface = promptPaymentIncentiveRepository.findOne(promptPaymentIncentiveInterface.getId());
        }
        return fetchedPromptPaymentIncentiveInterface;
    }

    @Override
    public PromptPaymentIncentiveInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        PromptPaymentIncentiveInterface promptPaymentIncentiveInterface = null;
        promptPaymentIncentiveInterface = promptPaymentIncentiveRepository.findOne(id);
        return promptPaymentIncentiveInterface;
    }
}
