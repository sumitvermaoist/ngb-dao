package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.SecurityDepositDAOInterface;
import com.mppkvvcl.ngbdao.repositories.SecurityDepositRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 11/17/2017.
 */
@Service
public class SecurityDepositDAO implements SecurityDepositDAOInterface<SecurityDepositInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(SecurityDepositDAO.class);

    @Autowired
    private SecurityDepositRepository securityDepositRepository;

    @Override
    public SecurityDepositInterface add(SecurityDepositInterface securityDepositToAdd) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        SecurityDepositInterface insertedSecurityDeposit = null;
        if (securityDepositToAdd != null){
            insertedSecurityDeposit = securityDepositRepository.save(securityDepositToAdd);
        }
        return insertedSecurityDeposit;
    }

    @Override
    public List<SecurityDepositInterface> add(List<SecurityDepositInterface> securityDepositsToAdd) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        List<SecurityDepositInterface> insertedSecurityDeposits = null;
        if (securityDepositsToAdd != null){
            insertedSecurityDeposits = new ArrayList<>();
            for(SecurityDepositInterface securityDepositInterface : securityDepositsToAdd){
                SecurityDepositInterface insertedSecurityDeposit = add(securityDepositInterface);
                insertedSecurityDeposits.add(insertedSecurityDeposit);
            }
        }
        return insertedSecurityDeposits;
    }
    @Override
    public SecurityDepositInterface update(SecurityDepositInterface securityDepositToUpdate) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        SecurityDepositInterface updatedSecurityDeposit = null;
        if (securityDepositToUpdate != null){
            updatedSecurityDeposit = securityDepositRepository.save(securityDepositToUpdate);
        }
        return updatedSecurityDeposit;
    }

    @Override
    public SecurityDepositInterface get(SecurityDepositInterface securityDepositInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        SecurityDepositInterface existingSecurityDeposit = null;
        if (securityDepositInterface != null){
            existingSecurityDeposit = securityDepositRepository.findOne(securityDepositInterface.getId());
        }
        return existingSecurityDeposit;
    }

    @Override
    public SecurityDepositInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        SecurityDepositInterface existingSecurityDeposit = null;
        existingSecurityDeposit = securityDepositRepository.findOne(id);
        return existingSecurityDeposit;
    }

    @Override
    public List<SecurityDepositInterface> getByConsumerNoOrderByIdDesc(String consumerNo) {
        final String methodName = "getByConsumerNoOrderByIdDesc() : ";
        logger.info(methodName + "called");
        List<SecurityDepositInterface> fetchedSecurityDeposit = null;
        if(consumerNo != null){
            fetchedSecurityDeposit = securityDepositRepository.findByConsumerNoOrderByIdDesc(consumerNo);
        }
        return fetchedSecurityDeposit;
    }

    @Override
    public SecurityDepositInterface getTopByConsumerNoOrderByIdDesc(String consumerNo) {
        final String methodName = "getTopByConsumerNoOrderByIdDesc() : ";
        logger.info(methodName + "called");
        SecurityDepositInterface fetchedSecurityDeposit = null;
        if(consumerNo != null){
            fetchedSecurityDeposit = securityDepositRepository.findTopByConsumerNoOrderByIdDesc(consumerNo);
        }
        return fetchedSecurityDeposit;
    }

    @Override
    public List<SecurityDepositInterface> getByConsumerNoAndEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(String consumerNo, Date effectiveStartDate, Date effectiveEndDate)
    {
        final String methodName = "getByConsumerNoAndEffectiveStartDateAndEffectiveEndDate() : ";
        logger.info(methodName + "called");
        List<SecurityDepositInterface> securityDepositInterfaces = null;
        if(consumerNo != null && effectiveStartDate != null && effectiveEndDate != null){
            securityDepositInterfaces = securityDepositRepository.findConsumerNoAndEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(consumerNo, effectiveStartDate,effectiveEndDate);
        }
        return securityDepositInterfaces;
    }

}
