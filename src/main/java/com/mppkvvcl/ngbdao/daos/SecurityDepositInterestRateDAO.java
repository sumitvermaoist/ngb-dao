package com.mppkvvcl.ngbdao.daos;


import com.mppkvvcl.ngbdao.interfaces.SecurityDepositInterestRateDAOInterface;
import com.mppkvvcl.ngbdao.repositories.SecurityDepositInterestRateRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositInterestRateInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

@Service
public class SecurityDepositInterestRateDAO implements SecurityDepositInterestRateDAOInterface<SecurityDepositInterestRateInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(SecurityDepositInterestRateDAO.class);

    @Autowired
    private SecurityDepositInterestRateRepository securityDepositInterestRateRepository;


    @Override
    public List<SecurityDepositInterestRateInterface> getByEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(Date startDate, Date endDate) {
        final String methodName = "getByEffectiveStartDateAndEffectiveEndDate() : ";
        logger.info(methodName + "called");
        List<SecurityDepositInterestRateInterface> securityDepositInterestRateInterface = null;
        if(startDate != null && endDate != null){
            securityDepositInterestRateInterface = securityDepositInterestRateRepository.findByEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(startDate,endDate);
        }
        return securityDepositInterestRateInterface;
    }


    @Override
    public SecurityDepositInterestRateInterface add(SecurityDepositInterestRateInterface securityDepositInterestRateInterface) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        SecurityDepositInterestRateInterface insertedSecurityDepositInterestRate = null;
        if (securityDepositInterestRateInterface != null){
            insertedSecurityDepositInterestRate = securityDepositInterestRateRepository.save(securityDepositInterestRateInterface);
        }
        return insertedSecurityDepositInterestRate;
    }

    @Override
    public SecurityDepositInterestRateInterface update(SecurityDepositInterestRateInterface securityDepositInterestRateInterface) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        SecurityDepositInterestRateInterface updatedSecurityDepositInterestRate = null;
        if (securityDepositInterestRateInterface != null){
            updatedSecurityDepositInterestRate = securityDepositInterestRateRepository.save(securityDepositInterestRateInterface);
        }
        return updatedSecurityDepositInterestRate;
    }

    @Override
    public SecurityDepositInterestRateInterface get(SecurityDepositInterestRateInterface securityDepositInterestRateInterface) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        SecurityDepositInterestRateInterface existingSecurityDepositInterest = null;
        if (securityDepositInterestRateInterface != null){
            existingSecurityDepositInterest = securityDepositInterestRateRepository.findOne(securityDepositInterestRateInterface.getId());
        }
        return existingSecurityDepositInterest;
    }

    @Override
    public SecurityDepositInterestRateInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        SecurityDepositInterestRateInterface existingSecurityDepositInterestRate = null;
        existingSecurityDepositInterestRate = securityDepositInterestRateRepository.findOne(id);
        return existingSecurityDepositInterestRate;
    }
}
