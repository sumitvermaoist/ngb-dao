package com.mppkvvcl.ngbdao.daos;

import com.mppkvvcl.ngbdao.interfaces.SlabDAOInterface;
import com.mppkvvcl.ngbdao.repositories.SlabRepository;
import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbinterface.interfaces.SlabInterface;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
@Service
public class SlabDAO implements SlabDAOInterface<SlabInterface> {

    /**
     * To get Logger object for logging in current class.
     */
    private Logger logger = GlobalResources.getLogger(SlabDAO.class);

    @Autowired
    private SlabRepository slabRepository;


    @Override
    public SlabInterface add(SlabInterface slab) {
        final String methodName = "add() : ";
        logger.info(methodName + "called");
        SlabInterface insertedSlab = null;
        if (slab != null){
            insertedSlab = slabRepository.save(slab);
        }
        return insertedSlab;
    }

    @Override
    public SlabInterface update(SlabInterface slab) {
        final String methodName = "update() : ";
        logger.info(methodName + "called");
        SlabInterface updatedSlab = null;
        if (slab != null){
            updatedSlab = slabRepository.save(slab);
        }
        return updatedSlab;
    }

    @Override
    public SlabInterface get(SlabInterface slab) {
        final String methodName = "get() : ";
        logger.info(methodName + "called");
        SlabInterface existingSlab = null;
        if (slab != null){
            existingSlab = slabRepository.findOne(slab.getId());
        }
        return existingSlab;
    }

    @Override
    public SlabInterface getById(long id) {
        final String methodName = "getById() : ";
        logger.info(methodName + "called");
        SlabInterface existingSlab = null;
        if(id != 0){
            existingSlab = slabRepository.findOne(id);
        }
        return existingSlab;
    }

    @Override
    public List<SlabInterface> getByTariffId(long tariffId) {
        final String methodName = "getByTariffId() : ";
        logger.info(methodName + "called");
        List<SlabInterface> existingSlab = null;
        if(tariffId != 0){
            existingSlab = slabRepository.findByTariffId(tariffId);
        }
        return existingSlab;
    }

    @Override
    public List<SlabInterface> getByTariffIdAndSlabIdOrderById(long tariffId, String slabId) {
        final String methodName = "getByTariffIdAndSlabIdOrderById() : ";
        logger.info(methodName + "called");
        List<SlabInterface> fetchedSlab = null;
        if(tariffId != 0 && slabId != null ){
            fetchedSlab = slabRepository.findByTariffIdAndSlabIdOrderById(tariffId , slabId);
        }
        return fetchedSlab;
    }
}
