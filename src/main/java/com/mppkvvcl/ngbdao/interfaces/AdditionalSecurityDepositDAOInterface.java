package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by PREETESH on 11/17/2017.
 */
public interface AdditionalSecurityDepositDAOInterface<T> extends DAOInterface<T>  {

    public List<T> getByConsumerNo(String consumerNo);

    public T getByConsumerNoAndBillMonth(String consumerNo, String billMonth);
}
