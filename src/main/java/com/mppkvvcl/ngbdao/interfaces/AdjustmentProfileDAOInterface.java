package com.mppkvvcl.ngbdao.interfaces;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ANSHIKA on 15-09-2017.
 */
public interface AdjustmentProfileDAOInterface<T> extends DAOInterface<T>{
    public List<T> getByAdjustmentId(long adjustmentId);
}
