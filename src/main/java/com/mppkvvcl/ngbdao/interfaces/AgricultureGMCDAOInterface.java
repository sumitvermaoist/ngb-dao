package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

public interface AgricultureGMCDAOInterface<T> extends DAOInterface<T> {
    public T getByTariffIdAndSubcategoryCodeAndBillMonth(long tariffId, long subcategoryCode,String billMonth);

    public List<T> getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode);
}
