package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 9/15/2017.
 */
public interface CashWindowStatusDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByUsernameAndStatus(String username, String status);
    public List<T> getByLocationCodeAndDateAndStatus(String location, Date date, String status);
    public T getByLocationCodeAndWindowNameAndDate(String locationCode, String windowName, Date freezeOnDate);
    public List<T> getByUsernameAndStatusAndDate(String username, String status, Date freezeOnDate);
    public List<T> getByLocationCodeAndStatus(String locationCode, String statusOpen);
}
