package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
public interface ConsumerConnectionInformationDAOInterface<T> extends DAOInterface<T> {
    public T getByConsumerNo(String consumerNo);
    public List<T> getByTariffCategory(String tariffCategory);
    public List<T> getByTariffCategoryAndConnectionType(String tariffCategory, String connectionType);
    public List<T> getByIsXray(String isXray);
    public List<T> getByIsWeldingTransformerSurcharge(String isWeldingTransformerSurcharge);
    public List<T> getByIsCapacitorSurcharge(String isCapacitorSurcharge);
    public List<T> getByIsDemandside(String isDemandSide);
    public List<T> getByIsGovernment(String isGovernment);
    public List<T> getBySubCategoryCode(Long subCategoryCode);

}
