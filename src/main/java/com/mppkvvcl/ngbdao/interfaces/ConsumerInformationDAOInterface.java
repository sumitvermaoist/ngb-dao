package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by MPWZERP on 15-09-2017.
 */
public interface ConsumerInformationDAOInterface<T> extends DAOInterface<T> {
    public T getByConsumerNo(String consumerNo);
    public List<T> getByConsumerName(String consumerName);
}
