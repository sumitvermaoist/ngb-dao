package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;
import java.util.List;

/**
 * Created by Shivanshu on 15-09-2017.
 */
public interface ConsumerInformationHistoryDAOInterface<T> extends DAOInterface<T>{
    public List<T> getByConsumerNo(String consumerNo);
    public List<T> getByEndDateBetween(Date startDate , Date endingDate);
    public List<T> getByPropertyNameAndEndDateBetween(String propertyName, Date startingDate, Date endingDate);
}
