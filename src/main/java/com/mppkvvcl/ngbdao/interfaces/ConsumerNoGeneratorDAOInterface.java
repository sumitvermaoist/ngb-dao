package com.mppkvvcl.ngbdao.interfaces;
/**
 * Created by SHIVANSHU on 16-09-2017.
 */
public interface ConsumerNoGeneratorDAOInterface<T> extends DAOInterface<T>{

    public T getByLocationCode(String locationCode);
}
