package com.mppkvvcl.ngbdao.interfaces;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerNoMasterInterface;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
public interface ConsumerNoMasterDAOInterface<T> extends DAOInterface<T> {
    public T getByOldServiceNoOne(String oldServiceNoOne);
    public T getByOldServiceNoTwo(String oldServiceNoTwo);
    public T getByConsumerNo(String consumerNo);

    public long countByGroupNo(String groupNo);

    public long countByGroupNoAndReadingDiaryNo(String groupNo,String readingDiaryNo);

    public long countByGroupNoAndReadingDiaryNoAndStatus(String groupNo, String readingDiaryNo, String status);

    public long countByGroupNoAndStatus(String groupNo,String status);

    public long countByLocationCode(String locationCode);

    public long countByLocationCodeAndStatus(String locationCode,String status);


    public T getByOldServiceNoOneAndLocationCode(String oldServiceNoOne, String locationCode);

}
