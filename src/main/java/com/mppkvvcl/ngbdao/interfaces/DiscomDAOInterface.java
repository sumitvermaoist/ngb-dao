package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by SHIVANHSU on 16-09-2017.
 */
public interface DiscomDAOInterface<T> extends DAOInterface<T> {
    public List<? extends T> getAll();
}
