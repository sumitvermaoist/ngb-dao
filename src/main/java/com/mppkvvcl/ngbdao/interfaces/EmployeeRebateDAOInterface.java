package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;

public interface EmployeeRebateDAOInterface<T> extends DAOInterface<T>{
    public T getByCodeAndDate(String code,Date date);
}
