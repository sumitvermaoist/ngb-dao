package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by PREETESH on 10/3/2017.
 */
public interface GroupDAOInterface<T> extends DAOInterface<T> {

    public List<T> getByLocationCode(String locationCode);

    public T getByGroupNo(String groupNo);
}
