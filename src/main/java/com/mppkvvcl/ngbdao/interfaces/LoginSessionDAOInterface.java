package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

public interface LoginSessionDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByUsername(String username);
    public List<T> getByIp(String ip);
    public List<T> getByUsernameAndIp(String username, String ip);
    public List<T> getByBrowserName(String browserName);
}
