package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by SHIVANSHU on 16-09-2017.
 */
public interface MeterMasterDAOInterface<T> extends  DAOInterface<T> {

    public T getByIdentifier(String identifier);
    public List<T> getBySerialNo(String serialNo);
}
