package com.mppkvvcl.ngbdao.interfaces;

public interface MinimumChargeDAOInterface<T> extends DAOInterface<T> {
    public T getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode);
}
