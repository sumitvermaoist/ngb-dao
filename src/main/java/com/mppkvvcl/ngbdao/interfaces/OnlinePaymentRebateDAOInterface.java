package com.mppkvvcl.ngbdao.interfaces;
import java.util.Date;

public interface OnlinePaymentRebateDAOInterface<T> extends DAOInterface<T> {
    public T getByDate(Date date);
}
