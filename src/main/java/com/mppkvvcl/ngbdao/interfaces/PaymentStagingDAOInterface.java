package com.mppkvvcl.ngbdao.interfaces;

import java.util.Date;
import java.util.List;

public interface PaymentStagingDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByIvrs(String ivrs);
    public List<T> getByLocationCodeAndStatus(String locationCode, String status);
    public List<T> getByLocationCode(String locationCode);
    public List<T> getByStatus(String status);
    public List<T> getByDateOfTransaction(Date dateOfTransaction);
    public List<T> getByServiceNo(String status);
    public T getByTransactionId(String transactionId);
    public List<T> getByLocationCodeAndDateOfTransactionAndStatus(String locationCode,Date dateOfTransaction, String status);
    public List<T> getByDateOfTransactionAndStatus(Date dateOfTransaction, String status);
    public List<T> getByIvrsAndStatus(String ivrs, String status);
}
