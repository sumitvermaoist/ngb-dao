package com.mppkvvcl.ngbdao.interfaces;
import java.util.Date;

public interface PromptPaymentIncentiveDAOInterface<T> extends DAOInterface<T> {
    public T getByDate(Date date);
}
