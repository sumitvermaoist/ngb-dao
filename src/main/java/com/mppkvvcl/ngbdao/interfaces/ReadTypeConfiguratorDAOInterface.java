package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface ReadTypeConfiguratorDAOInterface<T> extends DAOInterface<T>{

    public T getByTariffIdAndSubcategoryCode(Long tariffId, Long subcategoryCode);

    public List<? extends T> getAll();
}
