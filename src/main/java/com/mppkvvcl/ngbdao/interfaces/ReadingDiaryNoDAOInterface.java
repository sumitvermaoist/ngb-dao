package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by PREETESH on 10/3/2017.
 */
public interface ReadingDiaryNoDAOInterface <T> extends DAOInterface<T>  {
    public List<T> getByGroupNo(String groupNo);

    public T getByGroupNoAndReadingDiaryNo(String groupNo,String readingDiaryNo);
}
