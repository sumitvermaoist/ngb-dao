package com.mppkvvcl.ngbdao.interfaces;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 11/17/2017.
 */

public interface SecurityDepositInterestDAOInterface<T> extends DAOInterface<T> {
    public List<T> getByConsumerNo(String consumerNo);
    public T getTopByConsumerNoOrderByIdDesc(String consumerNo);
    public List<T> add(List<T> list);
    public List<T> getTop2ByConsumerNoOrderByIdDesc(String consumerNo);
   }
