package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface SlabDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByTariffId(long tariffId);

    List<T> getByTariffIdAndSlabIdOrderById(long tariffId, String slabId);
}
