package com.mppkvvcl.ngbdao.interfaces;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by MITHLESH on 9/16/2017.
 */
public interface SubCategoryDAOInterface<T> extends DAOInterface<T>{

    public List<T> getByTariffId(Long tariffId);

    public List<T> getByTariffIdAndPremiseType(Long tariffId, String premiseType);

    public List<T> getByTariffIdAndConnectedLoadAndPremiseType(long tariffId, BigDecimal connectedLoad, String premiseType);

    public List<T> getByTariffIdAndConnectedLoadAndPremiseTypeAndApplicantType(long tariffId, BigDecimal connectedLoad, String premiseType, String applicantType);

    public List<T> getByTariffIdAndConnectedLoadAndContractDemandAndPremiseTypeAndApplicantType(long tariffId, BigDecimal connectedLoad, BigDecimal contractDemand, String premiseType, String applicantType);

    public T getTopByCodeOrderByIdDesc(long code);

    public T getByTariffIdAndCode(long tariffId, long code);
}
