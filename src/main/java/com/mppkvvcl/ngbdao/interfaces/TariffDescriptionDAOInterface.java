package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

public interface TariffDescriptionDAOInterface<T>  extends DAOInterface<T>{
    public T getByTariffCategory(String tariffCategory);
    public List<? extends T> getAll();
}
