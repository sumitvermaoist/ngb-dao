package com.mppkvvcl.ngbdao.interfaces;

import java.util.List;

/**
 * Created by PREETESH on 12/18/2017.
 */
public interface XrayConnectionFixedChargeRateDAOInterface <T> extends DAOInterface<T> {


    public List<T> getByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode);

    public T getByTariffIdAndSubcategoryCodeAndXrayType(long tariffId, long subcategoryCode, String xrayType);

}
