package com.mppkvvcl.ngbdao.interfaces;
import java.util.List;

/**
 * Created by RUPALI on 9/16/2017.
 */
public interface ZoneDAOInterface<T> extends DAOInterface<T> {
    public T getByLocationCode(String locationCode);
    public List<T> getByDivisionId(long divisionId);
    public T getByShortCode(String shortCode);

}
