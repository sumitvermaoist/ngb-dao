package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.AdjustmentTypeInterface;
import com.mppkvvcl.ngbentity.beans.AdjustmentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by SHIVANSHU on 14-07-2017.
 */
@Repository
public interface AdjustmentTypeRepository extends JpaRepository<AdjustmentType, Long>{
    public List<AdjustmentTypeInterface> findAllByView(boolean view);
    public AdjustmentTypeInterface findByCode(int code);
    public AdjustmentTypeInterface save(AdjustmentTypeInterface adjustmentTypeInterface);
}
