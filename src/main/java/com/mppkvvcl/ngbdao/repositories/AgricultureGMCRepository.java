package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.AgricultureGMC;
import com.mppkvvcl.ngbinterface.interfaces.AgricultureGMCInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgricultureGMCRepository extends JpaRepository<AgricultureGMC,Long> {

    @Query("from AgricultureGMC agmc where agmc.tariffId = :tariffId and agmc.subcategoryCode = :subcategoryCode and (to_date(:billMonth,'MON-YYYY') >= to_date(agmc.startMonth,'MON-YYYY') and to_date(:billMonth,'MON-YYYY') <= to_date(agmc.endMonth,'MON-YYYY'))")
    public AgricultureGMCInterface findByTariffIdAndSubcategoryCodeAndBillMonth(@Param("tariffId") long tariffId,@Param("subcategoryCode") long subcategoryCode,@Param("billMonth") String billMonth);

    public List<AgricultureGMCInterface> findByTariffIdAndSubcategoryCode(long tariffId,long subcategoryCode);

    public AgricultureGMCInterface save(AgricultureGMCInterface agricultureGMCInterface);

}
