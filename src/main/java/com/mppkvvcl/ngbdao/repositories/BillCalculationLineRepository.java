package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.BillCalculationLine;
import com.mppkvvcl.ngbinterface.interfaces.BillCalculationLineInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 12/12/2017.
 */
@Repository
public interface BillCalculationLineRepository extends JpaRepository<BillCalculationLine, Long> {

    public BillCalculationLineInterface save(BillCalculationLineInterface BillCalculationLineInterface);
    
    public List<BillCalculationLineInterface> findByBillId(long billId);
}
