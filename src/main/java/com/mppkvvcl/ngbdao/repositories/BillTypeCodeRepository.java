package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.BillTypeCode;
import com.mppkvvcl.ngbinterface.interfaces.BillTypeCodeInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillTypeCodeRepository extends JpaRepository<BillTypeCode,Long> {
    public BillTypeCodeInterface findByCode(String code);
    public BillTypeCodeInterface save(BillTypeCodeInterface billTypeCodeInterface);
}
