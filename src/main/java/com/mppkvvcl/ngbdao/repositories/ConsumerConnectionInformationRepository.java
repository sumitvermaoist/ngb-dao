package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionInformationInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerConnectionInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by SUMIT on 14-06-2017.
 */
@Repository
public interface ConsumerConnectionInformationRepository  extends JpaRepository<ConsumerConnectionInformation,Long>{

    public ConsumerConnectionInformationInterface findByConsumerNo(String consumerNo);
    public List<ConsumerConnectionInformationInterface> findByTariffCategory(String tariffCategory);
    public List<ConsumerConnectionInformationInterface> findByTariffCategoryAndConnectionType(String tariffCategory, String connectionType);
    public List<ConsumerConnectionInformationInterface> findByIsXray(String isXray);
    public List<ConsumerConnectionInformationInterface> findByIsWeldingTransformerSurcharge(String isWeldingTransformerSurcharge);
    public List<ConsumerConnectionInformationInterface> findByIsCapacitorSurcharge(String isCapacitorSurcharge);
    public List<ConsumerConnectionInformationInterface> findByIsDemandside(String isDemandSide);
    public List<ConsumerConnectionInformationInterface> findByIsGovernment(String isGovernment);
    public List<ConsumerConnectionInformationInterface> findBySubCategoryCode(Long subCategoryCode);

    public ConsumerConnectionInformationInterface save(ConsumerConnectionInformationInterface consumerConnectionInformation);
}
