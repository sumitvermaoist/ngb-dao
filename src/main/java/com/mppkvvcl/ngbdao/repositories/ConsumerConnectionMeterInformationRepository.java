package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionMeterInformationInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerConnectionMeterInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 6/14/2017.
 */
@Repository
public interface ConsumerConnectionMeterInformationRepository extends JpaRepository<ConsumerConnectionMeterInformation,Long> {
   public ConsumerConnectionMeterInformationInterface findByConsumerNo(String consumerNo);

   public ConsumerConnectionMeterInformationInterface save(ConsumerConnectionMeterInformationInterface consumerConnectionMeterInformation);
}
