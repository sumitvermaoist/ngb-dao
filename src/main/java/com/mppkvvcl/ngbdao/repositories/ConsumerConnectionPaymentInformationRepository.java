package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerConnectionPaymentInformationInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerConnectionPaymentInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PREETESH on 6/14/2017.
 */
@Repository
public interface ConsumerConnectionPaymentInformationRepository extends JpaRepository<ConsumerConnectionPaymentInformation,Long> {
    public ConsumerConnectionPaymentInformationInterface save(ConsumerConnectionPaymentInformationInterface consumerConnectionPaymentInformation);
}
