package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerISIEnergySavingMappingInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerISIEnergySavingMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by SHIVANSHU on 6/14/2017.
 */
@Repository
public interface ConsumerISIEnergySavingMappingRepository extends JpaRepository<ConsumerISIEnergySavingMapping,Long> {
    public ConsumerISIEnergySavingMappingInterface save(ConsumerISIEnergySavingMappingInterface consumerISIEnergySavingMapping);
}
