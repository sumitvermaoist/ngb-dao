package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.ConsumerNoGeneratorInterface;
import com.mppkvvcl.ngbentity.beans.ConsumerNoGenerator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by SUMIT on 20-06-2017.
 */
@Repository
public interface ConsumerNoGeneratorRepository extends JpaRepository<ConsumerNoGenerator,Long>{
public ConsumerNoGeneratorInterface findByLocationCode(String locationCode);

    public ConsumerNoGeneratorInterface save(ConsumerNoGeneratorInterface consumerNoGenerator);
}
