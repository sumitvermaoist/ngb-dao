package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.DiscomInterface;
import com.mppkvvcl.ngbentity.beans.Discom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by SHIVANSHU on 17-07-2017.
 */
@Repository
public interface DiscomRepository extends JpaRepository<Discom, Long>{
    DiscomInterface save (DiscomInterface discomInterface);
}
