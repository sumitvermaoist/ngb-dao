package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.EmployeeConnectionMappingInterface;
import com.mppkvvcl.ngbentity.beans.EmployeeConnectionMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by Vikas Patel on 6/23/2017.
 */
@Repository
public interface EmployeeConnectionMappingRepository extends JpaRepository<EmployeeConnectionMapping,Long> {

    /**
     * to get Employee Connection Mapping by giving Employee no
     * param employeeNo String
     * return  List of EmployeeConnectionMapping
     */
    public List<EmployeeConnectionMappingInterface> findByEmployeeNo(String employeeNo);

    /**
     * to get Employee Connection Mapping by giving Consumer no
     * param consumerNo String
     * return List of EmployeeConnectionMapping
     */
    public List<EmployeeConnectionMappingInterface> findByConsumerNo(String consumerNo);

    /**
     * to get Employee Connection Mapping by giving Status
     * param status String
     * return  List of EmployeeConnectionMapping
     */
    public List<EmployeeConnectionMappingInterface> findByStatus(String status);

    /**
     * to get Employee Connection Mapping by giving employee no and Status
     * param employeeNo String
     * param Status String
     * return List of EmployeeConnectionMapping
     */
    public List<EmployeeConnectionMappingInterface> findByEmployeeNoAndStatus(String employeeNo, String Status);

    /**
     * to get Employee Connection Mapping by giving consumer no and Status
     * param consumerNo String
     * param Status String
     * return List of EmployeeConnectionMapping
     */
    public List<EmployeeConnectionMappingInterface> findByConsumerNoAndStatus(String consumerNo, String Status);

    /**
     * to get Employee Connection Mapping by giving consumer no and employee no and Status
     * param employeeNo String
     * param Status String
     * return List of EmployeeConnectionMapping
     */
    public List<EmployeeConnectionMappingInterface> findByEmployeeNoAndConsumerNoAndStatus(String employeeNo,String consumerNo,String Status);

    /**
     * to get Employee Connection Mapping by giving Start date period
     * param startDate Date
     * param endDate  Date
     * return  List of EmployeeConnectionMapping
     */
    public List<EmployeeConnectionMappingInterface> findByStartDateBetween(Date startDate, Date endDate);

    /**
     * to get Employee Connection Mapping by giving End date period
     * param startDate
     * param endDate
     * return  List of EmployeeConnectionMapping
     */
    public List<EmployeeConnectionMappingInterface> findByEndDateBetween(Date startDate, Date endDate);

    /**
     * to get details of employee connection mapping by given start/end Start_Bill_month
     * param startBillMonth
     * param endBillMonth
     * return  List of EmployeeConnectionMapping
     */
    @Query("from EmployeeConnectionMapping ecm where to_date(ecm.startBillMonth,'MON-YYYY') between to_date(:startMonth,'MON-YYYY') and to_date(:endMonth,'MON-YYYY')")
    public  List<EmployeeConnectionMappingInterface> findByStartBillMonthBetween(@Param ("startMonth") String startBillMonth, @Param("endMonth")String endBillMonth);

    /**
     * To get details of employee connection mapping by given start/end END_Bill_month
     * param startBillMonth
     * param endBillMonth
     * return  List of EmployeeConnectionMapping
     */
    @Query("from EmployeeConnectionMapping ecm where to_date(ecm.endBillMonth,'MON-YYYY') between to_date(:startMonth,'MON-YYYY') and to_date(:endMonth,'MON-YYYY')")
    public  List<EmployeeConnectionMappingInterface> findByEndBillMonthBetween(@Param ("startMonth") String startBillMonth, @Param("endMonth")String endBillMonth);


    public EmployeeConnectionMappingInterface save(EmployeeConnectionMappingInterface adjustmentInterface);

}
