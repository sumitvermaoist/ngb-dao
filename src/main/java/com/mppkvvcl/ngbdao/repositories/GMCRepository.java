package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.GMCInterface;
import com.mppkvvcl.ngbentity.beans.GMC;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by ANSHIKA on 27-07-2017.
 */
@Repository
public interface GMCRepository extends JpaRepository<GMC, Long> {
    public GMCInterface save(GMCInterface gmc);

    public GMCInterface findBySubcategoryCodeAndTariffId(long subcategoryCode, long tariffId) ;
}
