package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.HolidayInterface;
import com.mppkvvcl.ngbentity.beans.Holiday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Created by PREETESH on 9/7/2017.
 */
@Repository
public interface HolidayRepository extends JpaRepository<Holiday, Long>{

    public HolidayInterface findByDate(Date date);
    public HolidayInterface save(HolidayInterface holidayInterface);
}
