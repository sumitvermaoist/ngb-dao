package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.InstrumentDishonourInterface;
import com.mppkvvcl.ngbentity.beans.InstrumentDishonour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PREETESH on 7/31/2017.
 */
@Repository
public interface InstrumentDishonourRepository extends JpaRepository<InstrumentDishonour, Long> {


    InstrumentDishonourInterface findTopByConsumerNoOrderByIdDesc(String consumerNo);
    InstrumentDishonourInterface save(InstrumentDishonourInterface instrumentDishonourInterface);
}
