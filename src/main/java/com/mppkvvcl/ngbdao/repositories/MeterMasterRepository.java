package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.MeterMasterInterface;
import com.mppkvvcl.ngbentity.beans.MeterMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by SUMIT on 20-06-2017.
 */
@Repository
public interface MeterMasterRepository extends JpaRepository<MeterMaster,Long>{
    public MeterMasterInterface findByIdentifier(String identifier);
    public List<MeterMasterInterface> findBySerialNo(String serialNo);
    public MeterMasterInterface save(MeterMasterInterface meterMasterInterface);
}
