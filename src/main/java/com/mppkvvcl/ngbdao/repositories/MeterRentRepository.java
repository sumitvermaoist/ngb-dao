package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.MeterRentInterface;
import com.mppkvvcl.ngbentity.beans.MeterRent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by Vikas Patel on 6/21/2017.
 */

@Repository
public interface MeterRentRepository extends JpaRepository<MeterRent,Long> {

    /**
     * to get details of meter rent and effective date by given meter code
     * param meterCode String
     * return List of MeterRent

     */
    public MeterRentInterface findByMeterCodeAndMeterCapacity(String meterCode, String meterCapacity);

    /**
     *
     * param meterCode
     * return
     */
    public List<MeterRentInterface> findByMeterCode(String meterCode);


    /**
     * to get details of meter code and effective date by given meter rent
     * param meterRent
     * return
     */
    public List<MeterRentInterface> findByMeterRent(BigDecimal meterRent);

    /**
     * to get details of meter rent by given meter code and effective date
     * param meterCode String and effective Date date
     * param effectiveDate
     * return MeterRent
     */
    public MeterRentInterface findByMeterCodeAndEffectiveStartDate(String meterCode,Date effectiveStartDate);

    /**
     * to get details of meter rent by given meter code and effective date
     * param meterRent String and effective Date date
     * return MeterRent
     */
    public MeterRentInterface findByMeterRentAndEffectiveStartDate(BigDecimal meterRent, Date effectiveStartDate);

    public MeterRentInterface save(MeterRentInterface meterRentInterface);
}
