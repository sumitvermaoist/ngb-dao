package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.NSCStaging;
import com.mppkvvcl.ngbinterface.interfaces.NSCStagingInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Created by NITISH on 31-05-2017.
 * Repository Interface of NSCStaging Table at the backend database
 * for various CRUD Application on that table.
 */
@Repository
public interface NSCStagingRepository extends JpaRepository<NSCStaging,Long>{
    public NSCStagingInterface findOne(long id);
    public NSCStagingInterface save(NSCStagingInterface nscStagingInterface);

    public boolean existsByLocationCodeAndConnectionDateBetween(String locationCode, Date fromDate, Date toDate);
}
