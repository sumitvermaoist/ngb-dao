package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.Payment;
import com.mppkvvcl.ngbinterface.interfaces.PaymentInterface;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
/**
 * Created by ANSHIKA on 14-07-2017.
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

    public long countByConsumerNo(String consumerNo);

    public List<PaymentInterface> findByConsumerNo(String consumerNo);

    /**
     * Pageable Method returns list on the basis of pageable properties coded by nitish
     */
    public List<PaymentInterface> findByConsumerNo(String consumerNo, Pageable pageable);

    @Query("from #{#entityName} p where p.consumerNo= :consumerNo order by to_date(p.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
    public List<PaymentInterface> findByConsumerNoOrderByPostingBillMonthAscendingWithPageable(@Param("consumerNo") String consumerNo, Pageable pageable);

    @Query("from #{#entityName} p where p.consumerNo= :consumerNo order by to_date(p.postingBillMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
    public List<PaymentInterface> findByConsumerNoOrderByPostingBillMonthDescendingWithPageable(@Param("consumerNo") String consumerNo, Pageable pageable);

    public List<PaymentInterface> findByLocationCodeAndPostingBillMonth(String locationCode, String postingBillMonth);

    public List<PaymentInterface> findByConsumerNoAndDeletedAndPosted(String consumerNo, boolean deleted, boolean posted);

    public List<PaymentInterface> findByLocationCodeAndPayWindowAndPayDateAndDeleted(String locationCode, String payWindow, Date payDate, boolean deleted);

    public List<PaymentInterface> findByConsumerNoAndPostingBillMonthAndDeletedAndPosted(String consumerNo, String billMonth, boolean deleted, boolean posted);

    public PaymentInterface save(PaymentInterface paymentInterface);

    public List<PaymentInterface> findByConsumerNoAndPayModeIn(String consumerNo, String[] payModes);

    public List<PaymentInterface> findByConsumerNoAndDeleted(String consumerNo, boolean deleted);

    /*public List<PaymentInterface> save(List<PaymentInterface> paymentInterfaces);*/
}