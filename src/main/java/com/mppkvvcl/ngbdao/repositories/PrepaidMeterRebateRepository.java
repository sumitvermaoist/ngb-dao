package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.PrepaidMeterRebate;
import com.mppkvvcl.ngbinterface.interfaces.PrepaidMeterRebateInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.Date;

@Repository
public interface PrepaidMeterRebateRepository extends JpaRepository<PrepaidMeterRebate,Long>{

    @Query("from PrepaidMeterRebate pmr where :date >= pmr.effectiveStartDate and :date <= pmr.effectiveEndDate")
    public PrepaidMeterRebateInterface findByDate(@Param("date") Date date);

    public PrepaidMeterRebateInterface save(PrepaidMeterRebateInterface prepaidMeterRebateInterface);
}
