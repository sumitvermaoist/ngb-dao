package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.PurposeOfInstallationInterface;
import com.mppkvvcl.ngbentity.beans.PurposeOfInstallation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Preeteh
 * Edited by Nitish on 25/05/2017 14:57
 * Repository Interface of PurposeOfInstallation Table at the backend database
 * for various CRUD Application on that table.
 *
 * Edited by SUMIT on  26-05-2017 11:34
 * changed tariffCategory in service methods to tariffCode as per discussions done on 26-05-2017
 */
@Repository
public interface PurposeOfInstallationRepository extends JpaRepository<PurposeOfInstallation, Long> {

    /**
     * method to fetch various purpose of installations under passed tariff category
     * param tariffCode
     * return List of PurposeOfInstallation
     */
    public List<PurposeOfInstallationInterface> findByTariffCode(String tariffCode);

    /**
     * method to fetch purpose of installation data as per passed purposeOfInstallation
     * param purposeOfInstallation
     * return PurposeOfInstallation
     */
    public PurposeOfInstallationInterface findByPurposeOfInstallation(String purposeOfInstallation);

    public PurposeOfInstallationInterface save(PurposeOfInstallationInterface purposeOfInstallationInterface);
}
