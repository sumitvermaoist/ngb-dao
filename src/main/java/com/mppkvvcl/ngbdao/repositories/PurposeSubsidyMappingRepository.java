package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.PurposeSubsidyMapping;
import com.mppkvvcl.ngbinterface.interfaces.PurposeSubsidyMappingInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Date;

@Repository
public interface PurposeSubsidyMappingRepository extends JpaRepository<PurposeSubsidyMapping,Long>{

    @Query("from #{#entityName} p where p.subcategoryCode = :subcategoryCode and p.purposeOfInstallationId = :purposeOfInstallationId " +
            "and (:date >= p.effectiveStartDate and :date <= p.effectiveEndDate)")
    public List<PurposeSubsidyMappingInterface> findBySubcategoryCodeAndPurposeOfInstallationIdAndEffectiveStartDateAndEffectiveEndDate
            (@Param("subcategoryCode") long subcategoryCode, @Param("purposeOfInstallationId") long purposeOfInstallationId, @Param("date") Date date);

    @Query("from #{#entityName} p where p.subcategoryCode = :subcategoryCode " +
            "and (:date >= p.effectiveStartDate and :date <= p.effectiveEndDate)")
    public List<PurposeSubsidyMappingInterface> findBySubcategoryCodeAndEffectiveStartDateAndEffectiveEndDate
            (@Param("subcategoryCode") long subcategoryCode, @Param("date") Date date);

    public PurposeSubsidyMappingInterface save(PurposeSubsidyMappingInterface purposeSubsidyMappingInterface);

}
