package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.RegionInterface;
import com.mppkvvcl.ngbentity.beans.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by MITHLESH on 18-07-2017.
 */
@Repository
public interface RegionRepository extends JpaRepository<Region, Long>{

    RegionInterface save(RegionInterface regionInterface);
}
