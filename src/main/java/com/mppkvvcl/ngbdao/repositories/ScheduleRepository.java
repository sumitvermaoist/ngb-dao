package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbdao.utility.GlobalResources;
import com.mppkvvcl.ngbentity.beans.Schedule;
import com.mppkvvcl.ngbinterface.interfaces.ScheduleInterface;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by vikas patel on 6/30/2017.
 */

@Repository
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

    public ScheduleInterface save(ScheduleInterface scheduleInterface);

    /**
     * to get Schedule information by  providing Group No
     * param groupNo
     * return List of Schedule
     */
    public List<ScheduleInterface> findByGroupNoOrderByIdDesc(String groupNo);

    /**
     * to get Schedule information by  providing Bill Month
     * param billMonth
     * return List of Schedule
     */
    public List<ScheduleInterface> findByBillMonth(String billMonth);

    /**
     * to get Schedule information by  providing Bill Status
     * param billStatus
     * return  List of Schedule
     */
    public List<ScheduleInterface> findByBillStatus(String billStatus);

    /**
     * to get Schedule information by  providing Bill month and Bill Status
     * param billMonth
     * param billStatus
     * return
     */
    public List<ScheduleInterface> findByBillMonthAndBillStatus(String billMonth,String billStatus);

    /**
     * to get Schedule information by  providing Bill Status and Submitted (y/n)
     * param billStatus
     * param submitted
     * return
     */
    public List<ScheduleInterface> findByBillStatusAndSubmitted(String billStatus,String submitted);

    /**
     * to get Schedule information by  providing R15 status
     * param r15Status
     * return
     */
    public List<ScheduleInterface> findByR15Status(String r15Status);

    /**
     * to get Schedule information by  providing group no, Bill Month and Bill Status
     * param groupNo
     * param billMonth
     * param billStatus
     * return
     */
    public ScheduleInterface findByGroupNoAndBillMonthAndBillStatus(String groupNo, String billMonth, String billStatus);

    /**
     * to get Schedule information by  providing Bill month, Bill status and Submitted (y/n)
     * param billMonth
     * param billStatus
     * param submitted
     * return
     */
    public List<ScheduleInterface> findByBillMonthAndBillStatusAndSubmitted(String billMonth, String billStatus, String submitted);

    /**
     * to get Schedule information by  providing Bill month and R15Status
     * @param billMonth
     * @param r15Status
     * @return
     */
    public List<ScheduleInterface> findByBillMonthAndR15Status(String billMonth,String r15Status);

    /**
     * to get Schedule information matching GroupNo and Bill Status and Submitted (y/n)
     * param groupNo
     * param billStatus
     * param submitted
     * return
     */
    public List<ScheduleInterface> findByGroupNoAndBillStatusAndSubmitted(String groupNo, String billStatus, String submitted);

    /**
     * to get the latest reading by groupNo
     * param groupNo
     * return
     */
    public ScheduleInterface findTopByGroupNoOrderByIdDesc(String groupNo);

    /**
     *
     * param groupNo
     * param billStatus
     * return
     */
    public ScheduleInterface findTopByGroupNoAndBillStatusOrderByIdDesc(String groupNo,String billStatus);

    public ScheduleInterface findByGroupNoAndBillMonth(String groupNo, String lastBillMonth);

    /**
     * To count number of schedule created for
     * @param groupNo
     * @return
     */
    public long countByGroupNo(String groupNo);

    public List<ScheduleInterface> findTop2ByGroupNoOrderByIdDesc(String groupNo);

    public ScheduleInterface findByGroupNoAndBillMonthAndBillStatusAndSubmitted(String groupNo,String previousBillMonth,String billStatus,String submittedStatus);

    //pageable methods
    public List<ScheduleInterface> findByGroupNo(String groupNo, Pageable pageable);

    @Query("from #{#entityName} s where s.groupNo= :groupNo order by to_date(s.billMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') DESC")
    public List<ScheduleInterface> findByGroupNoOrderByBillMonthDescending(@Param("groupNo") String groupNo, Pageable pageable);

    @Query("from #{#entityName} s where s.groupNo= :groupNo order by to_date(s.billMonth,'" + GlobalResources.BILL_MONTH_FORMAT_FOR_POSTGRESQL_DB + "') ASC")
    public List<ScheduleInterface> findByGroupNoOrderByBillMonthAscending(@Param("groupNo") String groupNo, Pageable pageable);
}
