package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.SecurityDepositPosted;
import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositPostedInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PREETESH on 11/17/2017.
 */

@Repository
public interface SecurityDepositPostedRepository  extends JpaRepository<SecurityDepositPosted, Long> {

    public SecurityDepositPostedInterface save(SecurityDepositPostedInterface securityDepositPostedInterface);
    public List<SecurityDepositPostedInterface> findBySecurityDepositId(long securityDepositID);
    public List<SecurityDepositPostedInterface> findByBillMonth(String billMonth);
    public List<SecurityDepositPostedInterface> save(List<SecurityDepositPostedInterface> securityDepositPostedInterfaces);
}
