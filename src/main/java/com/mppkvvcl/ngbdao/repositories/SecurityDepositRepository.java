package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.SecurityDeposit;
import com.mppkvvcl.ngbinterface.interfaces.SecurityDepositInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by PREETESH on 11/17/2017.
 */
@Repository
public interface SecurityDepositRepository extends JpaRepository<SecurityDeposit, Long> {

    public SecurityDepositInterface save(SecurityDepositInterface securityDepositInterface);
    public List<SecurityDepositInterface> findByConsumerNoOrderByIdDesc(String consumerNo);
    public SecurityDepositInterface findTopByConsumerNoOrderByIdDesc(String consumerNo);

    @Query("from SecurityDeposit sd where sd.consumerNo = :consumerNo and ((:startDate >= sd.effectiveStartDate and :startDate <= sd.effectiveEndDate) or (:endDate >= sd.effectiveStartDate and :endDate <= sd.effectiveEndDate) or (:startDate <= sd.effectiveStartDate and :endDate >= sd.effectiveEndDate)) order by id asc")
    public  List<SecurityDepositInterface> findConsumerNoAndEffectiveStartDateAndEffectiveEndDateOrderByIdAsc(@Param("consumerNo") String consumerNo, @Param("startDate") Date startDate,@Param("endDate") Date endDate);

}
