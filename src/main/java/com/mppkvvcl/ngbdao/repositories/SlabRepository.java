package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.SlabInterface;
import com.mppkvvcl.ngbentity.beans.Slab;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by MITHLESH on 14-07-2017.
 */
@Repository
public interface SlabRepository extends JpaRepository<Slab, Long>{
    public List<SlabInterface> findByTariffId(long tariffId);

    public List<SlabInterface> findByTariffIdAndSlabIdOrderById(long tariffId, String slabId);

    public SlabInterface save(SlabInterface slabInterface);
}
