package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.SurchargeInterface;
import com.mppkvvcl.ngbentity.beans.Surcharge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface SurchargeRepository extends JpaRepository<Surcharge,Long>{
    public List<SurchargeInterface> findByTariffIdAndSubcategoryCode(long tariffId, long subcategoryCode);

    public List<SurchargeInterface> findByTariffIdAndSubcategoryCodeAndOutstandingAmountGreaterThanEqual(long tariffId, long subcategoryCode, BigDecimal outstandingAmount);

    public SurchargeInterface save(SurchargeInterface surchargeInterface);
}
