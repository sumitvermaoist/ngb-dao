package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.TDS;
import com.mppkvvcl.ngbinterface.interfaces.TDSInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TDSRepository extends JpaRepository<TDS,Long> {

    public TDSInterface findBySecurityDepositInterestId(long securityDepositInterestId);

    public TDSInterface save(TDSInterface tdsInterface);
}
