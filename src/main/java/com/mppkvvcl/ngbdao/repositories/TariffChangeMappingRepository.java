package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbinterface.interfaces.TariffChangeMappingInterface;
import com.mppkvvcl.ngbentity.beans.TariffChangeMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by vikas on 9/20/2017.
 */

@Repository

public interface TariffChangeMappingRepository extends JpaRepository<TariffChangeMapping, Long> {

    public List<TariffChangeMappingInterface> findByTariffCategory(String tariffCategory);

    public TariffChangeMappingInterface save(TariffChangeMappingInterface tariffChangeMappingInterface);

}
