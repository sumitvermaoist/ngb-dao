package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.TariffDetail;
import com.mppkvvcl.ngbinterface.interfaces.TariffDetailInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by RUPALI on 27-07-2017.
 */
@Repository
public interface TariffDetailRepository extends JpaRepository<TariffDetail, Long> {

    public List<TariffDetailInterface> findByConsumerNoOrderByIdDesc(String consumerNo);

    public TariffDetailInterface findTopByConsumerNoOrderByIdDesc(String ConsumerNo);

    public TariffDetailInterface save(TariffDetailInterface tariffDetailInterface);

    @Query("from TariffDetail t where t.consumerNo = :consumerNo and t.effectiveStartDate <= :effectiveStartDate " +
            " and t.effectiveEndDate  > :effectiveStartDate or t.effectiveStartDate <= :effectiveEndDate " +
            " and t.effectiveEndDate > :effectiveEndDate order by t.id desc")
    public List<TariffDetailInterface> findByConsumerNoAndEffectiveStartDateAndEffectiveEndDate(@Param("consumerNo") String consumerNo, @Param("effectiveStartDate") Date effectiveStartDate, @Param("effectiveEndDate") Date effectiveEndDate);


  //  public List<TariffDetailInterface> findByConsumerNoAndEffectiveStartDateAfterAndEffectiveStartDateBefore(String ConsumerNo , Date startDate , Date endDate);

}
