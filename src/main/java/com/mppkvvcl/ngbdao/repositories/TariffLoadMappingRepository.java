package com.mppkvvcl.ngbdao.repositories;

import com.mppkvvcl.ngbentity.beans.TariffLoadMapping;
import com.mppkvvcl.ngbinterface.interfaces.TariffLoadMappingInterface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TariffLoadMappingRepository extends JpaRepository<TariffLoadMapping,Long> {

    public TariffLoadMappingInterface save(TariffLoadMappingInterface tariffLoadMappingInterface);

    public List<TariffLoadMappingInterface> findByTariffDetailId(long tariffId);

    public List<TariffLoadMappingInterface> findByLoadDetailId(long loadId);

    public List<TariffLoadMappingInterface> findByTariffDetailIdAndLoadDetailId(long tariffId, long loadId);

    public List<TariffLoadMappingInterface> findByTariffDetailIdAndLoadDetailIdAndIsTariffChangeAndIsLoadChange(long tariffId, long loadId, boolean isTariffChange, boolean isLoadChange);

    public List<TariffLoadMappingInterface> findByTariffDetailIdAndLoadDetailIdAndIsTariffChange(long tariffId, long loadId, boolean isTariffChange);

    public TariffLoadMappingInterface findTopByTariffDetailIdOrderByIdDesc(long tariffId);

    public TariffLoadMappingInterface findTopByLoadDetailIdOrderByIdDesc(long loadId);
}